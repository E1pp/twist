#pragma once

/*
 * Annotations for data race checks
 *
 * twist::assist::Shared<T>
 *
 * Usage:
 *
 * twist::assist::Shared<int> var;
 *
 * *var = 1;
 * int v = var;
 *
 * // Capture source location
 * var.Write(1);
 * int v = var.Read();
 *
 * twist::assist::Shared<Widget> w;
 *
 * w->Foo();
 * w.WriteView()->Foo();  // Capture source location
 *
 */

#include <twist/rt/facade/assist/non_atomic.hpp>

namespace twist::assist {

template <typename T>
using Shared = rt::fiber::user::assist::NonAtomic<T>;

}  // namespace twist::assist
