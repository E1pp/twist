#pragma once

/*
 * Annotations for memory checks
 *
 * void twist::assist::MemoryAccess(void* addr, size_t size);
 *
 * twist::assist::Ptr<T>
 *
 * Usage:
 *
 * Node* top = top_.load(std::memory_order::acquire);
 * Node* top_next = twist::assist::Ptr(top)->next;
 *
 */

#include <twist/rt/facade/assist/memory.hpp>

namespace twist::assist {

using rt::facade::assist::New;
using rt::facade::assist::MemoryAccess;
using rt::facade::assist::Ptr;

}  // namespace twist::assist
