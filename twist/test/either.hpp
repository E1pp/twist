#pragma once

/*
 * bool twist::test::Either();
 *
 * Usage:
 *
 * if (twist::test::Either()) {
 *   stack.Push(1);
 * } else {
 *   stack.TryPop();
 * }
 *
 */

#include <twist/ed/random/choice.hpp>

namespace twist::test {

inline bool Either() {
  return twist::ed::random::Choose(/*alts=*/2) == 0;
}

}  // namespace twist::test
