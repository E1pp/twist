#pragma once

#include <string>

#include <wheels/core/source_location.hpp>
#include <wheels/memory/view.hpp>

#include <fmt/core.h>
#include <fmt/format.h>

#include <algorithm>

namespace twist::test {

namespace log {

bool Enabled();

wheels::MutableMemView FmtBuffer();

void Log(wheels::SourceLocation loc, std::string_view message);

}  // namespace log

}  // namespace twist::test

#if defined(__TWIST_FAULTY__)

#define TWIST_LOG(...) \
  do {                 \
    if (::twist::test::log::Enabled()) { \
      auto buffer = ::twist::test::log::FmtBuffer(); \
      auto fmt_result = fmt::format_to_n(buffer.Data(), buffer.Size(), __VA_ARGS__); \
                       \
      size_t message_size = std::min(buffer.Size(), fmt_result.size);                 \
      std::string_view message_view{buffer.Data(), message_size};                    \
                       \
      ::twist::test::log::Log(WHEELS_HERE, message_view); \
    }                   \
  } while (false)

#else

#define TWIST_LOG(...) do { } while (false)

#endif
