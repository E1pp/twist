#pragma once

#include "../simulator.hpp"
#include "replay.hpp"

#include <twist/rt/fiber/scheduler/random/scheduler.hpp>

#include <optional>

namespace twist::sim::sched {

using RandomScheduler = twist::rt::fiber::system::scheduler::random::Scheduler;

}  // namespace twist::sim::sched
