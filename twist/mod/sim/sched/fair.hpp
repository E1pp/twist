#pragma once

#include "../simulator.hpp"

#include <twist/rt/fiber/scheduler/fair/scheduler.hpp>

namespace twist::sim::sched {

using FairScheduler = twist::rt::fiber::system::scheduler::fair::Scheduler;

}  // namespace twist::sim::sched
