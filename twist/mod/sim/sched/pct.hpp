#pragma once

#include "../simulator.hpp"

#include <twist/rt/fiber/scheduler/pct/scheduler.hpp>

#include <optional>

namespace twist::sim::sched {

using PctScheduler = twist::rt::fiber::system::scheduler::pct::Scheduler;

}  // namespace twist::sim::sched
