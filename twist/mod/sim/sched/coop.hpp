#pragma once

#include "../simulator.hpp"

#include <twist/rt/fiber/scheduler/coop/scheduler.hpp>

namespace twist::sim::sched {

using CoopScheduler = twist::rt::fiber::system::scheduler::coop::Scheduler;

}  // namespace twist::sim::sched
