#pragma once

#include "../simulator.hpp"

#include <twist/rt/fiber/scheduler/replay/scheduler.hpp>
#include <twist/rt/fiber/scheduler/replay/recorder.hpp>

namespace twist::sim::sched {

using Schedule = twist::rt::fiber::system::scheduler::replay::Schedule;
using Recorder = twist::rt::fiber::system::scheduler::replay::Recorder;
using ReplayScheduler = twist::rt::fiber::system::scheduler::replay::Scheduler;

}  // namespace twist::sim::sched
