#pragma once

#include "simulator.hpp"
#include "sched/replay.hpp"

namespace twist::sim {
namespace sched {

void Print(MainRoutine main, Schedule schedule, SimulatorParams params = SimulatorParams{});

}  // namespace sched
}  // namespace twist::sim
