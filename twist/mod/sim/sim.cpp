#include "det.hpp"
#include "explore.hpp"

#include "sched/coop.hpp"
#include "sched/fair.hpp"
#include "sched/random.hpp"
#include "sched/pct.hpp"
#include "sched/dfs.hpp"

#include <random>

#if defined(__TWIST_FIBERS_ISOLATE_USER_MEMORY__)
#include <twist/rt/fiber/system/memory/isolated/static.hpp>
#endif

namespace twist::sim {

//

Digest DetCheckSim(SimulatorParams params, MainRoutine main) {
  static const size_t kIters = 512;

  sched::RandomScheduler scheduler{};
  Simulator simulator{&scheduler, params};

  simulator.Silent(true);
  simulator.Start(main);
  simulator.RunFor(kIters);
  auto result = simulator.Burn();

  return result.digest;
}

bool DetCheck(SimulatorParams params, MainRoutine main) {
  {
    /*
     * /\ Isolated user memory
     * /\ Same memory mapping for same process
     */
    params.digest_atomic_loads = true;
    // Do not crash in determinism check
    params.crash_on_abort = false;
    params.forward_stdout = false;
  }

  Digest d1 = DetCheckSim(params, main);
  Digest d2 = DetCheckSim(params, main);

  if (d1 != d2) {
    return false;
  }

  Digest d3 = DetCheckSim(params, main);

  if (d3 != d1) {
    return false;
  }

  return true;
}

//

namespace sched {

Schedule Record(IScheduler& scheduler, SimulatorParams params, MainRoutine main, Digest digest) {
  Recorder recorder{&scheduler};
  Simulator simulator{&recorder, params};
  auto result = simulator.Run(main);
  WHEELS_VERIFY(result.digest == digest, "Digest mismatch for recorder");

  auto schedule = recorder.GetSchedule();

  {
    // Test replay
    ReplayScheduler replay_scheduler{schedule};
    Simulator replay_simulator{&replay_scheduler, params};
    auto replay_result = replay_simulator.Run(main);
    WHEELS_VERIFY(replay_result.digest == digest, "Digest mismatch for replay");
  }

  return recorder.GetSchedule();
}

void Print(MainRoutine main, Schedule schedule, SimulatorParams params) {
  ReplayScheduler scheduler{schedule};
  Simulator simulator{&scheduler, params};
  simulator.Silent(true);
  simulator.Print(true);
  auto result = simulator.Run(main);
}

}  // namespace sched

//

ExploreResult Explore(IScheduler& scheduler, SimulatorParams params, MainRoutine main) {
  if (!DetCheck(params, main)) {
    wheels::Panic("Simulation is not deterministic");
  }

  {
    params.forward_stdout = false;
    params.crash_on_abort = false;
  }

  size_t count = 0;

  do {
    Simulator sim{&scheduler, params};

    auto result = sim.Run(main);
    ++count;

    if (result.Failure()) {
      auto replay_schedule = sched::Record(scheduler, params, main, result.digest);

      return ExploreResult{
          .found ={{replay_schedule, result}},
          .sims=count,
      };
    }
  } while (scheduler.NextSchedule());

  return {{}, count};
}

ExploreResult Explore(IScheduler& scheduler, MainRoutine main) {
  return Explore(scheduler, {}, main);
}

void AssignMemoryRange(void* addr, size_t size) {
#if defined(__TWIST_FIBERS_ISOLATE_USER_MEMORY__)
  twist::rt::fiber::system::memory::isolated::StaticMemoryMapper()->Assign(addr, size);
#else
  wheels::Panic("Not supported, set TWIST_FIBERS_ISOLATE_USER_MEMORY CMake flag");
#endif
}

}  // namespace twist::sim
