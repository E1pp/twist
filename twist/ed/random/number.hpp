#pragma once

#include <twist/rt/facade/random/number.hpp>

namespace twist::ed {

namespace random {

using rt::facade::random::Number;

}  // namespace random

}  // namespace twist::ed
