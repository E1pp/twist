#pragma once

#include <twist/rt/facade/wait/spin.hpp>

namespace twist::ed {

// Busy waiting

// Usage: examples/spin/main.cpp

/*
 * void SpinLock::Lock() {
 *   // One-shot
 *   twist::ed::SpinWait spin_wait;
 *
 *   while (locked_.exchange(true)) {
 *     spin_wait();  // operator()() <- exponential backoff
 *   }
 * }
 *
 */

using rt::facade::SpinWait;
using rt::facade::CpuRelax;

}  // namespace twist::ed
