#pragma once

#include <twist/rt/thread/fault/adversary/adversary.hpp>

namespace twist::rt::thread {
namespace fault {

IAdversaryPtr CreateNopAdversary();

}  // namespace fault
}  // namespace twist::rt::thread
