#include "futex.hpp"

namespace twist::rt::thread::fault {

namespace futex {

using WaitableAtomic = FaultyAtomic<uint32_t>;

WaitableAtomic::Impl& Impl(WaitableAtomic& atomic) {
  static_assert(sizeof(WaitableAtomic) == sizeof(WaitableAtomic::Impl));

  return reinterpret_cast<WaitableAtomic::Impl&>(atomic);
}

void Wait(FaultyAtomic<uint32_t>& atomic, uint32_t old, std::memory_order mo) {
  rt::thread::futex::Wait(Impl(atomic), old, mo);
}

bool WaitTimed(FaultyAtomic<uint32_t>& atomic, uint32_t old,
               std::chrono::milliseconds timeout, std::memory_order mo) {
  return strand::futex::WaitTimed(Impl(atomic), old, timeout, mo);
}

WakeKey PrepareWake(FaultyAtomic<uint32_t>& atomic) {
  return rt::thread::futex::PrepareWake(Impl(atomic));
}

void WakeOne(WakeKey key) {
  rt::thread::futex::WakeOne(key);
}

void WakeAll(WakeKey key) {
  rt::thread::futex::WakeAll(key);
}

}  // namespace futex

}  // namespace twist::rt::thread::fault
