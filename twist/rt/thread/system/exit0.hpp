#pragma once

#include <cstdlib>

namespace twist::rt::thread {

[[noreturn]] inline void Exit0() {
  std::exit(0);
}

}  // namespace twist::rt::thread
