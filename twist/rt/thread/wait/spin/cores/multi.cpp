#include <twist/rt/thread/wait/spin/cores/multi.hpp>

#include <twist/rt/thread/wait/spin/relax.hpp>

namespace twist::rt::thread {

namespace cores::multi {

void SpinWait::Spin() {
  static const size_t kYieldThreshold = 10;

  for (size_t j = 0; j < ((size_t)1 << spins_); ++j) {
    CpuRelax();
  }

  if (spins_ > kYieldThreshold) {
    std::this_thread::yield();
  } else {
    ++spins_;
  }
}

}  // namespace cores::multi

}  // namespace twist::rt::thread
