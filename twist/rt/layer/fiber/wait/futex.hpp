#pragma once

#include <twist/rt/layer/fiber/stdlike/atomic.hpp>

#include <twist/rt/layer/fiber/wait/wake_key.hpp>

#include <chrono>

namespace twist::rt::fiber {

namespace futex {

// Wait

void Wait(stdlike::Atomic<uint32_t>& atomic, uint32_t old,
          std::memory_order mo = std::memory_order::seq_cst);

bool WaitTimed(stdlike::Atomic<uint32_t>& atomic, uint32_t old,
               std::chrono::milliseconds timeout,
               std::memory_order mo = std::memory_order::seq_cst);

// Wake

WakeKey PrepareWake(stdlike::Atomic<uint32_t>& atomic);

void WakeOne(WakeKey key);
void WakeAll(WakeKey key);

}  // namespace futex

}  // namespace twist::rt::fiber
