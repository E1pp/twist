#include "am_i_user.hpp"

#include <twist/rt/fiber/system/simulator.hpp>

namespace twist::rt::fiber {

namespace user {

bool AmIUser() {
  return system::Simulator::Current()->AmIUser();
}

}  // namespace user

}  // namespace twist::rt::fiber
