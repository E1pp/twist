#pragma once

#include <twist/rt/fiber/system/thread/id.hpp>

namespace twist::rt::fiber {

namespace user::syscall {

system::ThreadId GetId();

}  // namespace user::syscall

}  // namespace twist::rt::fiber
