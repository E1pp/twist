#pragma once

#include <twist/rt/fiber/system/sync/action.hpp>

namespace twist::rt::fiber {

namespace user::syscall {

uint64_t Sync(system::sync::Action* action);

}  // namespace user::syscall

}  // namespace twist::rt::fiber
