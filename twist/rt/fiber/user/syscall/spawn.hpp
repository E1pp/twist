#pragma once

#include <twist/rt/fiber/system/thread/id.hpp>
#include <twist/rt/fiber/system/thread/user.hpp>

namespace twist::rt::fiber {

namespace user::syscall {

system::ThreadId Spawn(system::IThreadUserState*);
void Detach(system::ThreadId);

}  // namespace user::syscall

}  // namespace twist::rt::fiber
