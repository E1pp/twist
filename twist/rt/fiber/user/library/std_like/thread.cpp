#include "thread.hpp"

// #include <wheels/core/assert.hpp>
#include <twist/rt/fiber/user/safety/assert.hpp>

#include <fmt/core.h>

// std::exchange
#include <utility>

namespace twist::rt::fiber {

namespace user::library::std_like {

// ThreadId

ThreadId::ThreadId()
    : fid(system::kImpossibleThreadId) {
}

std::ostream& operator<<(std::ostream& out, ThreadId id) {
  out << ::fmt::format("{0:x}", id.fid);
  return out;
}

// Synchronization

void Thread::StateBase::Detach() {
  syscall::Detach(id.fid);

  // User-space coordination between parent and child
  if (state.fetch_sub(1) == 1) {
    // Last
    fmt::println("Delete thread state from Detach");
    delete this;  // Completed
  }
}

void Thread::StateBase::Join(wheels::SourceLocation call_site) {
  system::WaiterContext waiter{system::FutexType::Thread, "thread::join", call_site};
  while (state.load() == 2) {  // Not completed
    syscall::FutexWait(&state, 2, &waiter);
  }
  delete this;
}

void Thread::StateBase::AtThreadExit() noexcept {
  if (state.fetch_sub(1) == 1) {
    // Last ~ Detached
    delete this;  // Detached
  } else {
    syscall::FutexWake(&state, 1);
  }
}

// Thread

Thread::Thread()
    : state_(nullptr) {
}

Thread::Thread(StateBase* state, wheels::SourceLocation /*source_loc*/) {
  auto id = syscall::Spawn(state);
  state->id = id;
  state_ = state;
}

Thread::~Thread() {
  ___TWIST_FIBER_USER_VERIFY(!HasState(), "Thread join or detach required");
}

Thread::Thread(Thread&& that)
   : state_(that.ReleaseState()) {
}

Thread& Thread::operator=(Thread&& that) {
  ___TWIST_FIBER_USER_VERIFY(!HasState(), "Cannot rewrite joinable thread object");
  state_ = that.ReleaseState();
  return *this;
}

ThreadId Thread::get_id() const noexcept {
  if (HasState()) {
    return state_->id;
  } else {
    return ThreadId{};
  }
}

bool Thread::joinable() const {
  return HasState();
}

void Thread::detach() {
  ___TWIST_FIBER_USER_VERIFY(HasState(), "Thread has already been detached");
  ReleaseState()->Detach();
}

void Thread::join(wheels::SourceLocation call_site) {
  ___TWIST_FIBER_USER_VERIFY(HasState(), "Thread has been detached");
  ReleaseState()->Join(call_site);
}

unsigned int Thread::hardware_concurrency() noexcept {
  return 4;
}

Thread::native_handle_type Thread::native_handle() {
  return state_;
}

void Thread::swap(Thread& that) {
  std::swap(state_, that.state_);
}

bool Thread::HasState() const {
  return state_ != nullptr;
}

Thread::StateBase* Thread::ReleaseState() {
  return std::exchange(state_, nullptr);
}

}  // namespace user::library::std_like

}  // namespace twist::rt::fiber
