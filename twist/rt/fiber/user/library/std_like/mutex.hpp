#pragma once

#include "atomic.hpp"
#include "mutex_owner.hpp"

#include <twist/rt/fiber/user/syscall/futex.hpp>

// #include <wheels/core/assert.hpp>
#include <twist/rt/fiber/user/safety/assert.hpp>

#include <twist/rt/fiber/user/scheduler/spurious.hpp>

// std::lock_guard, std::unique_lock
#include <mutex>
// std::exchange
#include <utility>

namespace twist::rt::fiber {

namespace user::library::std_like {

// ~ Atomic
class MutexImpl {
  enum State : uint64_t {
    Unlocked = 0,
    Locked = 1,
  };

 public:
  MutexImpl(wheels::SourceLocation source_loc) {
    {
      scheduler::PreemptionGuard g;  // ???

      system::sync::Action init{this, system::sync::ActionType::AtomicInit, State::Unlocked, std::memory_order::relaxed /* ignored */,
                                "mutex::mutex", source_loc};
      syscall::Sync(&init);
    }
    std::memset(buf_, 0, sizeof(uint32_t));
  }

  void Lock(wheels::SourceLocation call_site) {
    while (!TryLockImpl(call_site, "mutex::lock", /*spurious=*/false)) {
      system::WaiterContext waiter{system::FutexType::MutexLock, "mutex::lock", call_site};
      syscall::FutexWait(this, State::Locked, &waiter);
    }
  }

  bool TryLock(wheels::SourceLocation call_site) {
    return TryLockImpl(call_site, "mutex::try_lock", /*spurious=*/true);
  }

  void Unlock(wheels::SourceLocation call_site) {
    {
      system::sync::Action unlock{this, system::sync::ActionType::AtomicStore, State::Unlocked, std::memory_order::seq_cst,
                                 "atomic::store", call_site};
      syscall::Sync(&unlock);
    }
    syscall::FutexWake(this, 1);
  }

  ~MutexImpl() {

  }

 private:
  bool TryLockImpl(wheels::SourceLocation call_site, const char* op, bool spurious) {
    system::sync::Action try_lock{this, system::sync::ActionType::AtomicRmwLoad, /*ignored*/ 0, std::memory_order::seq_cst, op, call_site};
    uint64_t state = syscall::Sync(&try_lock);

    if (state == State::Unlocked) {
      if (spurious) {
        if (scheduler::SpuriousTryFailure()) {
          return false;
        }
      }

      scheduler::PreemptionGuard g;
      system::sync::Action commit{this, system::sync::ActionType::AtomicRmwCommit, State::Locked, std::memory_order::seq_cst, op, call_site};
      syscall::Sync(&commit);
    }

    return state == State::Unlocked;
  }

 private:
  alignas(uint32_t) char buf_[sizeof(uint32_t)];
};

class Mutex {
 public:
  Mutex(wheels::SourceLocation source_loc = wheels::SourceLocation::Current())
      : impl_(source_loc) {
  }

  // Non-copyable
  Mutex(const Mutex&) = delete;
  Mutex& operator=(const Mutex&) = delete;

  // Non-movable
  Mutex(Mutex&&) = delete;
  Mutex& operator=(Mutex&&) = delete;

  // std::mutex / Lockable

  void lock(wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {  // NOLINT
    impl_.Lock(call_site);
    owner_.Lock();
  }

  bool try_lock(wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {  // NOLINT
    bool acquired = impl_.TryLock(call_site);
    if (acquired) {
      owner_.Lock();
    }
    return acquired;
  }

  void unlock(wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {  // NOLINT
    owner_.Unlock();
    impl_.Unlock(call_site);
  }

 private:
  MutexImpl impl_;
  MutexOwner owner_;
};

}  // namespace user::library::std_like

}  // namespace twist::rt::fiber
