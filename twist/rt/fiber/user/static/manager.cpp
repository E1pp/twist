#include "manager.hpp"

#include <twist/rt/fiber/user/library/fmt/temp.hpp>
#include <twist/rt/fiber/user/safety/panic.hpp>

#include <twist/rt/fiber/system/simulator.hpp>

namespace twist::rt::fiber {

namespace user {

namespace ss {

bool Manager::IsMainRunning() const {
  return system::Simulator::TryCurrent() != nullptr;
}

void Manager::AbortOnException(IVar* global) {
  user::Panic(system::Status::UnhandledException,
      user::library::fmt::FormatTemp(
          "Exception thrown from constructor of static global variable '{}'",
          global->Name()),
      global->SourceLoc());
}

void* Manager::AllocVar(size_t size) {
  return system::Simulator::Current()->UserAllocStatic(size);
}

void Manager::DeallocVars() {
  return system::Simulator::Current()->UserDeallocStatics();
}

}  // namespace ss

}  // namespace user

}  // namespace twist::rt::fiber
