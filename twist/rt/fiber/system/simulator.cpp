#include <twist/rt/fiber/system/simulator.hpp>

#include <twist/rt/fiber/user/static/manager.hpp>
#include <twist/rt/fiber/system/futex/wake_count.hpp>
#include <twist/rt/fiber/system/thread_count.hpp>

#include <twist/wheels/stacktrace.hpp>
#include <twist/wheels/format.hpp>

#include <twist/rt/fiber/user/library/fmt/buf_writer.hpp>

#include <wheels/core/compiler.hpp>
#include <wheels/core/panic.hpp>

#include <fmt/core.h>

#include <vector>
#include <utility>
#include <iostream>
#include <set>

namespace twist::rt::fiber {

namespace system {

static const size_t kReportBufSize = 4096;
static char report_buf[kReportBufSize];

Simulator* Simulator::current = nullptr;

Simulator::Simulator(scheduler::IScheduler* scheduler, Params params,
                     ResourceManager* resource_manager)
    : params_(params),
      scheduler_(scheduler),
      resource_manager_(resource_manager ? resource_manager : StaticResourceManager()),
      time_(ticker_, params),
      memory_(params),
      memory_model_() {
  ValidateParams();
}

// Operations invoked by running threads

void Simulator::SwitchTo(Thread* thread) {
  loop_context_.SwitchTo(thread->context);
}

call::Status Simulator::SystemCall(call::Handler handler) {
#ifndef NDEBUG
  if (debug_) {
    DebugSwitch(running_);
  }
#endif

  syscall_ = handler;
  running_->context.SwitchTo(loop_context_);
  return running_->status;

#ifndef NDEBUG
  if (debug_) {
    DebugResume(running_);
  }
#endif
}

// ~ System calls

void Simulator::SysNoop() {
  auto noop = [](Thread*) {
    return true;  // Resume;
  };

  SystemCall(noop);
}

ThreadId Simulator::SysSpawn(IThreadUserState* user) {
  ThreadId id;

  auto spawn = [this, user, &id](Thread* me) {
    id = _SysSpawn(/*parent=*/me, user);
    return true;  // Continue;
  };

  SystemCall(spawn);

  return id;
}

ThreadId Simulator::SysGetId() {
  WHEELS_ASSERT(AmIUser(), "Not a user");
  // NB: Without context switch
  return RunningThread()->id;
}

void Simulator::SysDetach(ThreadId id) {
  auto detach = [this, id](Thread*) {
    _SysDetach(id);
    return true;  // Continue
  };

  SystemCall(detach);
}

void Simulator::SysYield() {
  auto yield = [this](Thread* me) {
    return _SysYield(me);
  };

  SystemCall(yield);
}

call::Status Simulator::SysSleepFor(Time::Duration delay) {
  return SysSleepUntil(time_.After(delay));
}

struct SleepTimer final : Timer {
  Thread* sleeper;

  void Alarm() noexcept override {
    sleeper->simulator->Wake(/*waker=*/nullptr, sleeper, WakeType::SleepTimer);
  }
};

call::Status Simulator::SysSleepUntil(Time::Instant deadline) {
  SleepTimer timer;
  timer.when = deadline;
  timer.sleeper = running_;

  auto sleep = [this, timer = &timer](Thread* me) -> bool {
    _SysSleep(me, timer);
    return false;  // Suspend
  };

  return SystemCall(sleep);
}

static bool IsUserAction(sync::Action* action) {
  return strstr(action->source_loc.File(), "twist/rt/fiber") == nullptr;
}

static void PrintSeparatorLine() {
  fmt::println("");
  fmt::println("------------------------------");
}

void Simulator::PrintSync(Thread* thread, sync::Action* action) {
  // In user space!

  PrintSeparatorLine();

  user::library::fmt::BufWriter writer{report_buf, kReportBufSize};

  writer.FormatAppend("[Thread #{}] Sync action {}", thread->id, action->op);

  if (sync::IsRmwStep(action->type)) {
    if (action->type == sync::ActionType::AtomicRmwLoad) {
      writer.FormatAppend(" / [R]mw");
    } else {
      writer.FormatAppend(" / rm[W]");
    }
  }

  writer.FormatAppend(" / addr = {:#x}", (uintptr_t)action->loc);

  if (sync::IsLoad(action->type)) {
    writer.FormatAppend(" -> {:#x}", thread->action_ret);
  } else if (sync::IsStore(action->type)) {
    writer.FormatAppend(" <- {:#x}", action->value);
  }

  writer.FormatAppend(" at {}:{} [{}]",
                      action->source_loc.File(), action->source_loc.Line(), action->source_loc.Function());

  fmt::println("{}\n", writer.StringView());

  {
    stacktrace_ = true;
    PrintStackTrace(std::cout);
    stacktrace_ = false;
  }
}

uint64_t Simulator::SysSync(sync::Action* action) {
  Thread* me = running_;

  auto sync = [this, action](Thread* me) {
    return _SysSync(me, action);
  };
  SystemCall(sync);

  if (print_ && IsUserAction(action)) {
    PrintSync(me, action);
  }

  return me->action_ret;
}

void Simulator::SysAccessNonAtomicVar(sync::Access access) {
  auto handler = [this, access](Thread* me) {
    _SysAccessNonAtomicVar(me, access);
    return true;
  };

  SystemCall(handler);
}

call::Status Simulator::SysFutexWait(void* var, uint64_t old, const WaiterContext* ctx) {
  auto wait = [this, var, old, ctx](Thread* me) {
    _SysFutexWait(me, var, old, /*timer=*/nullptr, ctx);
    return false;  // Suspend
  };

  Thread* me = running_;

  auto status = SystemCall(wait);

  if (WHEELS_UNLIKELY(me->state == ThreadState::Deadlocked)) {
    ReportFromDeadlockedThread(me);
    WHEELS_UNREACHABLE();
  }

  return status;
}

struct FutexTimer : Timer {
  Thread* waiter = nullptr;

  void Alarm() noexcept override {
    waiter->simulator->Wake(/*waker=*/nullptr, waiter, WakeType::FutexTimer);
  }
};

call::Status Simulator::SysFutexWaitTimed(void* var, uint64_t old, Time::Instant d, const WaiterContext* ctx) {
  FutexTimer timer;
  timer.when = d;
  timer.waiter = running_;

  auto wait_timed = [this, var, old, timer = &timer, ctx](Thread* me) {
    _SysFutexWait(me, var, old, timer, ctx);
    return false;  // Suspend
  };

  return SystemCall(wait_timed);
}

void Simulator::SysFutexWake(void* var, size_t count) {
  auto wake = [this, var, count](Thread* me) {
    _SysFutexWake(/*waker=*/me, var, count);
    return true;  // Continue
  };

  SystemCall(wake);
}

uint64_t Simulator::SysRandomNumber() {
  uint64_t number;

  auto random = [this, &number](Thread* /*me*/) {
    number = scheduler_->RandomNumber();
    return true;  // Resume
  };

  SystemCall(random);

  return number;
}

size_t Simulator::SysRandomChoice(size_t alts) {
  WHEELS_ASSERT(alts > 0, "alts == 0");

  if (alts == 1) {
    return 0;
  }

  size_t index;

  auto random = [this, alts, &index](Thread* /*me*/) {
    index = scheduler_->RandomChoice(alts);
    return true;  // Resume
  };

  SystemCall(random);

  return index;
}

bool Simulator::SysSpuriousTryFailure() {
  bool fail;

  auto failure = [this, &fail](Thread* /*me*/) {
    fail = scheduler_->SpuriousTryFailure();
    return true;  // Resume
  };

  SystemCall(failure);

  return fail;
}

void Simulator::SysLog(wheels::SourceLocation loc, std::string_view message) {
  if (silent_) {
    return;
  }

  // NB: wheels::SourceLocation does not own memory, so
  // capturing `loc` by value is ok
  auto log = [this, loc, message](Thread* me) {
    _SysLog(me, loc, message);
    return true;  // Resume
  };

  SystemCall(log);
}

void Simulator::SysWrite(int fd, std::string_view buf) {
  auto write = [this, fd, buf](Thread*) {
    _SysWrite(fd, buf);
    return true;  // Resume
  };

  SystemCall(write);

  if (print_) {
    PrintSeparatorLine();
    fmt::print("[Thread #{}] Write to fd {}: {}", running_->id, fd, buf);
  }
}

void Simulator::SysAbort(Status status) {
  auto abort = [this, status](Thread*) {
    _SysAbort(status);
    return false;  // Suspend
  };

  SystemCall(abort);  // Never returns

  WHEELS_UNREACHABLE();
}

void Simulator::SysSchedHintLockFree(bool flag) {
  auto hint = [this, flag](Thread* me) {
    scheduler_->LockFree(me, flag);
    return true;
  };

  SystemCall(hint);
}

void Simulator::SysSchedHintProgress() {
  auto hint = [this](Thread* me) {
    scheduler_->Progress(me);
    return true;  // Resume
  };

  SystemCall(hint);
}

void Simulator::SysSchedHintNewIter() {
  auto hint = [this](Thread*) {
    Checkpoint();
    scheduler_->NewIter();
    return true;  // Resume
  };

  SystemCall(hint);
}

void Simulator::SysThreadEnter() {
#ifndef NDEBUG
  if (debug_) {
    DebugStart(running_);
  }
#endif

  auto enter = [this](Thread* me) {
    _SysThreadEnter(me);
    return true;  // Resume
  };

  SystemCall(enter);
}

void Simulator::SysThreadExit() {
  auto exit = [this](Thread* me) {
    _SysThreadExit(me);
    return false;  // Suspend
  };

  SystemCall(exit);  // Never returns

  WHEELS_UNREACHABLE();
}

// Scheduling

Result Simulator::Run(MainRoutine main) {
  Init();
  StartLoop(std::move(main));
  RunLoop(LoopBudget::Unlimited());
  StopLoop();

  return Result{
    .status=*status_,
    .std_out=stdout_.str(),
    .std_err=stderr_.str(),
    .threads=ThreadCount(),
    .iters=IterCount(),
    .digest=digest_.Digest()
  };
}

void Simulator::Start(MainRoutine main) {
  Init();
  StartLoop(std::move(main));
}

size_t Simulator::RunFor(size_t iters) {
  return RunLoop({iters});
}

bool Simulator::RunOneIter() {
  return RunLoop({1}) == 1;
}

void Simulator::Drain() {
  RunLoop(LoopBudget::Unlimited());
  StopLoop();
}

Result Simulator::Burn() {
#if defined(__TWIST_FIBERS_ISOLATE_USER_MEMORY__)
  {
    // Burn threads
    std::vector<Thread*> alive;
    for (Thread* f : alive_) {
      alive.push_back(f);
    }
    for (Thread* f : alive) {
      Burn(f);
    }
  }

  {
    // Burn static variables
    user::ss::Manager::Instance().BurnVars();
  }

  {
    // Reset internal data structures
    timer_queue_.Reset();
    // memory_model_.Reset();  // Simulator is one-shot
  }

  {
    // Burn memory
    memory_.Burn();
  }

  ObjectAllocator()->Reset();

  ResetCurrentScheduler();

  done_ = true;

  return Result{
      .status=*status_,
      .std_out=stdout_.str(),
      .std_err=stderr_.str(),
      .threads=ThreadCount(),
      .iters=IterCount(),
      .digest=digest_.Digest(),
  };

#else
  WHEELS_PANIC("Scheduler::Burn not supported: set TWIST_FIBERS_ISOLATE_USER_MEMORY=ON");
#endif
}


void Simulator::ValidateParams() {
  // TODO
}

void Simulator::Init() {
  WHEELS_ASSERT(scheduler_ != nullptr, "Scheduler not set");

  memory_.Reset();

  if (params_.min_stack_size) {
    memory_.SetMinStackSize(*params_.min_stack_size);
  }

  time_.Reset();

  ids_.Reset();

  digest_.Reset();
  // Cache decision
  digest_atomic_loads_ = DigestAtomicLoads();

  // memory_model_.Reset();

  timer_queue_.Reset();  // timer ids

  done_ = false;

  {
    // Simulator is one-shot
    // std::stringstream{}.swap(stdout_);
    // std::stringstream{}.swap(stderr_);
  }
  status_.reset();

  burnt_threads_ = 0;

  iter_ = 0;
  last_checkpoint_ = 0;

  futex_wait_count_ = 0;
  futex_wake_count_ = 0;
}

// IUserRoutine
void Simulator::RunUser() {
  (*main_)();
}

void Simulator::AtThreadExit() noexcept {
  // No-op
}

void Simulator::SpawnMainThread(MainRoutine main) {
  main_ = std::move(main);

  Thread* main_thread = CreateThread(/*user=*/this);
  main_thread->main = true;
  scheduler_->Spawn(main_thread);
}

void Simulator::BurnDetachedThreads() {
  std::vector<Thread*> detached;

  for (Thread* f : alive_) {
    if (!f->main && !f->parent_id) {
      detached.push_back(f);
    }
  }

  for (Thread* f : detached) {
    Burn(f);
  }
}

void Simulator::MainCompleted() {
#if defined(__TWIST_FIBERS_ISOLATE_USER_MEMORY__)
  if (params_.allow_detached_threads) {
    BurnDetachedThreads();
  }
#endif

  WHEELS_VERIFY(alive_.IsEmpty(), "There are alive threads after main completion");

  CheckMemory();

  status_ = Status::Ok;
}

void Simulator::StartLoop(MainRoutine main) {
  SetCurrentScheduler();
  scheduler_->Start(this);
  SpawnMainThread(std::move(main));
}

void Simulator::SetCurrentScheduler() {
  WHEELS_VERIFY(current == nullptr, "Cannot run simulator from another simulator");
  current = this;
}

void Simulator::ResetCurrentScheduler() {
  current = nullptr;
}

scheduler::IRunQueue* Simulator::RunQueue() const {
  return scheduler_;
}

void Simulator::Tick() {
  ticker_.Tick();
}

void Simulator::Maintenance() {
  if (!timer_queue_.IsEmpty()) {
    PollTimers();
  }
#if defined(__TWIST_FAULTY__)
  if (!IsDeadlock()) {
    SpuriousWakeups();
  }
#endif
}

// Precondition: run queue is empty
// true - keep running, false - stop run loop
bool Simulator::Idle() {
  CheckDeadlock();

  if (timer_queue_.IsEmpty()) {
    return false;  // Stop
  }

  if (PollTimers()) {
    return true;  // Keep running
  }

  time_.AdvanceTo(timer_queue_.NextDeadLine());
  PollTimers();
  return true;
}

void Simulator::Run(Thread* thread) {
  thread->state = ThreadState::Running;
  ++thread->runs;
  running_ = thread;
  SwitchTo(thread);
  running_ = nullptr;
}

bool Simulator::Preemptive(bool on) {
  return std::exchange(RunningThread()->preemptive, on);
}

void Simulator::CompleteSync(Thread* thread) {
  sync::Action* action = std::exchange(thread->action, nullptr);
  if (action != nullptr) {
    thread->action_ret = Sync(thread, action);
  }
}

void Simulator::PrintPrevThreadStatus(ThreadId thread_id) {
  Thread* thread{nullptr};

  for (Thread* f : alive_) {
    if (f->id == thread_id) {
      thread = f;
      break;
    }
  }

  // Exit
  if (thread == nullptr) {
    fmt::println("[Simulator] Thread #{} terminated", thread_id);
    return;
  }

  // Futex
  if (thread->waiter != nullptr) {
    fmt::println("Thread #{} has been suspended ({}) at {}:{}",
                 thread_id,
                 thread->waiter->operation,
                 thread->waiter->source_loc.File(), thread->waiter->source_loc.Line());
    return;
  }

  // Synchronization
  if (thread->action != nullptr) {
    fmt::println("Thread #{} has been interrupted before {} operation at {}:{} [{}]",
               thread_id, thread->action->op,
                 thread->action->source_loc.File(), thread->action->source_loc.Line(), thread->action->source_loc.Function());
    return;
  }

  // Yield?
  fmt::println("Thread #{} has yielded", thread_id);
}

void Simulator::PrintIter(Thread* f) {
  if (prev_thread_id_ && f->id != *prev_thread_id_) {
    if (prev_thread_id_) {
      PrintSeparatorLine();
      PrintPrevThreadStatus(*prev_thread_id_);
    }

    PrintSeparatorLine();
    fmt::println("[Simulator] Switching to thread #{}", f->id);
  } else if (!prev_thread_id_) {
    fmt::println("[Simulator] Running thread #{}", f->id);
  }
}

void Simulator::Iter(Thread* f) {
  digest_.Iter(f);

  auto id = f->id;

  if (print_) {
    PrintIter(f);
  }

  CompleteSync(f);

  do {
    Run(f);
  } while (HandleSystemCall(f));

  prev_thread_id_ = id;
}

void Simulator::Iter() {
  ++iter_;

  Tick();

  if ((iter_ & 7) == 0) {
    Maintenance();
  }

  {
    Thread* next = RunQueue()->PickNext();
    Iter(next);
  }
}

struct SysAbortEx {
};

size_t Simulator::RunLoop(LoopBudget iters) {
  if (done_) {
    return 0;
  }

  size_t start_iter = iter_;

  try {
    do {
      while (!RunQueue()->IsIdle() && --iters) {
        Iter();
      }
    } while (iters && Idle());
  } catch (SysAbortEx) {
    Burn();
  }

  return (iter_ - start_iter);
}

bool Simulator::PollTimers() {
  bool progress = false;

  auto now = time_.Now();
  while (Timer* timer = timer_queue_.Poll(now)) {
    progress = true;
    timer->Alarm();
  }

  return progress;
}

void Simulator::AddTimer(Timer* timer) {
  timer_queue_.Add(timer);
}

void Simulator::CancelTimer(Timer* timer) {
  WHEELS_VERIFY(timer_queue_.Remove(timer), "Timer not found");
}

bool Simulator::HandleSystemCall(Thread* caller) {
  if (bool resume = syscall_(caller); resume) {
    caller->status = call::Status::Ok;
    return true;  // Resume caller
  } else {
    return false;  // Suspend caller
  }
}

ThreadId Simulator::_SysSpawn(Thread* parent, IThreadUserState* user) {
  Thread* child = CreateThread(user);
  child->parent_id = parent->id;
  memory_model_.SpawnFiber(parent, child);
  scheduler_->Spawn(child);
  return child->id;
}

void Simulator::_SysDetach(ThreadId id) {
  for (Thread* f : alive_) {
    if (f->id == id) {
      f->parent_id.reset();
    }
  }
}

bool Simulator::_SysYield(Thread* thread) {
  WHEELS_VERIFY(thread->preemptive, "Yield is not supported in non-preemptive mode");
  scheduler_->Yield(thread);
  return false;  // Always reschedule
}

void Simulator::_SysSleep(Thread* sleeper, Timer* timer) {
  AddTimer(timer);

  {
    sleeper->state = ThreadState::Sleeping;

    sleeper->futex = nullptr;
    sleeper->timer = timer;
    sleeper->waiter = nullptr;
  }
}


bool Simulator::_SysSync(Thread* thread, sync::Action* action) {
  thread->action = action;
  scheduler_->Interrupt(thread);
  return false;
}

void Simulator::_SysAccessNonAtomicVar(Thread* thread, sync::Access access) {
  auto race = memory_model_.AccessNonAtomicVar(thread, access);

  if (race) {
    auto type = [](sync::AccessType a) {
      return sync::IsRead(a) ? "READ" : "WRITE";
    };

    stderr_ << "Data race detected on memory location " << access.loc << ": " << std::endl;
    stderr_ << fmt::format("- Current {} from thread #{} at {}", type(access.type), thread->id, access.source_loc) << std::endl;
    stderr_ << fmt::format("- Previous {} from thread #{} at {}", type(race->type), race->thread, race->source_loc) << std::endl;

    _SysAbort(Status::DataRace);
  }
}

static bool VulnerableToSpuriousWakeup(const WaiterContext* waiter) {
  switch (waiter->type) {
    case FutexType::Futex:
    case FutexType::Atomic:
    case FutexType::CondVar:
    case FutexType::MutexTryLock:
      return true;
    default:
      return false;
  }
}

void Simulator::_SysFutexWait(Thread* waiter, void* var, uint64_t old, Timer* timer, const WaiterContext* ctx) {
  WHEELS_VERIFY(waiter->preemptive, "FutexWait is not supported in non-preemptive mode");

  if (VulnerableToSpuriousWakeup(ctx)) {
    if (scheduler_->SpuriousWakeup()) {
      ScheduleWaiter(waiter, call::Status::Interrupted);
      return;
    }
  }

  sync::AtomicVar* atomic = memory_model_.FutexAtomic(var);

  WHEELS_VERIFY(atomic != nullptr, "Atomic not found in SysFutexWait");

  if (memory_model_.FutexLoad(waiter, atomic) != old) {
    ScheduleWaiter(waiter, call::Status::Ok);
    return;
  }

  atomic->futex.Push(waiter);

  if (timer != nullptr) {
    AddTimer(timer);
  }

  {
    waiter->state = ThreadState::Parked;

    waiter->futex = atomic;
    waiter->timer = timer;
    waiter->waiter = ctx;
  }

  // Statistics
  ++futex_wait_count_;
}

void Simulator::_SysFutexWake(Thread* waker, void* var, size_t count) {
  sync::AtomicVar* atomic = memory_model_.TryFutexAtomic(var);
  if (atomic == nullptr) {
    return;
  }

  if (count == 1) {
    // One
    if (Thread* waiter = atomic->futex.Pop(); waiter != nullptr) {
      Wake(waker, waiter, WakeType::FutexWake);
    }
  } else if (count == FutexWakeCount::All) {
    // All
    while (Thread* waiter = atomic->futex.PopAll()) {
      Wake(waker, waiter, WakeType::FutexWake);
    }
  } else {
    WHEELS_PANIC("'count' value " << count << " not supported");
  }

  // Statistics
  ++futex_wake_count_;
}

void Simulator::_SysLog(Thread* logger, wheels::SourceLocation loc, std::string_view message) {
  LogEvent event{
    .source_loc=loc,
    .id=logger->id,
    .message=std::string{message},
  };

  logger_.Log(std::move(event));
};

void Simulator::_SysWrite(int fd, std::string_view buf) {
  if (fd == 1) {
    stdout_.write(buf.data(), buf.size());
    if (params_.forward_stdout && !silent_) {
      std::cout.write(buf.data(), buf.size());
    }
  } else if (fd == 2) {
    stderr_.write(buf.data(), buf.size());
  } else {
    // TODO: Abort simulation
    WHEELS_PANIC("Cannot write to fd " << fd);
  }
}

void Simulator::_SysAbort(Status status) {
#if defined(__TWIST_FIBERS_ISOLATE_USER_MEMORY__)
  status_ = status;
  throw SysAbortEx{};
#else
  WHEELS_UNUSED(status);  // TODO
  wheels::Panic(stderr_.str());
#endif
}

void Simulator::_SysThreadEnter(Thread* /*thread*/) {
  // No-op
}

void Simulator::_SysThreadExit(Thread* thread) {
  scheduler_->Exit(thread);

  bool main = thread->main;

  DestroyThread(thread);

  if (main) {
    MainCompleted();
  }
}

static bool IsDigestable(sync::ActionType action) {
  switch (action) {
    case sync::ActionType::AtomicLoad:
    case sync::ActionType::AtomicRmwLoad:
      return true;
    default:
      return false;
  }
}

void Simulator::DigestAtomicLoad(sync::ActionType action, uint64_t repr) {
  if (digest_atomic_loads_ && IsDigestable(action)) {
    digest_.Load(repr);
  }
}

uint64_t Simulator::Sync(Thread* thread, sync::Action* action) {
  uint64_t r = memory_model_.Sync(thread, action);

  DigestAtomicLoad(action->type, r);

  return r;
}

void Simulator::ScheduleWaiter(Thread* waiter, call::Status status) {
  waiter->status = status;
  scheduler_->Wake(waiter);
}

// From simulator context
void Simulator::Wake(Thread* waker, Thread* waiter, WakeType wake) {
  WHEELS_ASSERT(waiter->state == ThreadState::Parked || waiter->state == ThreadState::Sleeping,
                "Unexpected thread state");

  call::Status status;

  switch (wake) {
    case WakeType::SleepTimer:
      // SysSleepUntil -> Timeout
      status = call::Status::Ok;
      break;

    case WakeType::FutexWake:
      status = call::Status::Ok;
      memory_model_.WakeFiber(waker, waiter);
      if (waiter->timer != nullptr) {
        // SysFutexWaitTimed -> SysFutexWake
        CancelTimer(waiter->timer);
      } else {
        // Do nothing
      }
      break;

    case WakeType::FutexTimer:
      // SysFutexWaitTimed -> Timeout
      status = call::Status::Timeout;
      waiter->futex->futex.Remove(waiter);
      break;

    case WakeType::Spurious:
      // Spurious wake-up
      status = call::Status::Interrupted;
      if (waiter->futex != nullptr) {
        waiter->futex->futex.Remove(waiter);
      }
      if (waiter->timer != nullptr) {
        CancelTimer(waiter->timer);
      }

      break;
  }

  {
    // Reset thread state

    waiter->state = ThreadState::Runnable;

    waiter->futex = nullptr;
    waiter->timer = nullptr;
    waiter->waiter = nullptr;
  }

  ScheduleWaiter(waiter, status);
}

Thread* Simulator::CreateThread(IThreadUserState* state) {
  ++thread_count_;

  auto id = ids_.AllocateId();
  auto stack = memory_.AllocateStack(/*hint=*/id);

  // Kernel-space allocation
  Thread* thread = ObjectAllocator()->Allocate<Thread>();
  thread->Reset(this, state, std::move(stack), id);

  alive_.PushBack(thread);

  return thread;
}

void Simulator::DestroyThread(Thread* thread) {
  alive_.Unlink(thread);

  memory_.FreeStack(std::move(*(thread->stack)), /*hint=*/thread->id);
  ids_.Free(thread->id);

  ObjectAllocator()->Free(thread);
}

void Simulator::Burn(Thread* thread) {
  {
    RunQueue()->Remove(thread);
    if (thread->timer) {
      timer_queue_.Remove(thread->timer);
    }
    if (thread->futex) {
      thread->futex->futex.Remove(thread);
    }
  }

  ++burnt_threads_;

  DestroyThread(thread);
}

void Simulator::StopLoop() {
  WHEELS_VERIFY(RunQueue()->IsIdle(), "Broken scheduler");
  WHEELS_VERIFY(timer_queue_.IsEmpty(), "Broken scheduler");

  done_ = true;

  //memory_model_.Reset();  // Simulator is one-shot
  ObjectAllocator()->Reset();

  main_.reset();

  ResetCurrentScheduler();
}

void Simulator::CheckMemory() {
#if defined(__TWIST_FIBERS_ISOLATE_USER_MEMORY__)
  if (burnt_threads_ > 0) {
    return;  // Incompatible with memory leaks check
  }

  auto stat = memory_.HeapStat();

  if (stat.alloc_count != stat.free_count) {
    size_t bytes_leaked = stat.user_bytes_allocated - stat.user_bytes_freed;
    size_t leak_count = stat.alloc_count - stat.free_count;

    stderr_ << "Memory leaks: "
        << bytes_leaked << " bytes leaked" << " (" << leak_count << " allocation(s))";

    _SysAbort(Status::MemoryLeak);
  }
#endif
}

bool Simulator::IsDeadlock() const {
  return RunQueue()->IsIdle() &&
         timer_queue_.IsEmpty() &&
         alive_.NonEmpty();
}

void Simulator::CheckDeadlock() {
  if (IsDeadlock()) {
    // Validate
    for (Thread* f : alive_) {
      WHEELS_VERIFY(f->state == ThreadState::Parked, "Internal error");
    }
    ReportDeadlock();
    _SysAbort(Status::Deadlock);
  }
}

void Simulator::ReportDeadlock() {
  stderr_ << "Deadlock detected: "
          << alive_.Size() << " threads are blocked\n";

  stderr_ << fmt::format("Scheduler loop iterations: {}\n", iter_);
  if (last_checkpoint_ != 0) {
    stderr_ << fmt::format("Last checkpoint: iteration #{}\n", last_checkpoint_);
  }

  stderr_ << std::endl;  // Blank line

  for (Thread* thread : alive_) {
    thread->state = ThreadState::Deadlocked;
    running_ = thread;
    SwitchTo(thread);
    running_ = nullptr;
    std::string_view report = thread->report;
    stderr_.write(report.begin(), report.size());
  }
}

void Simulator::ReportFromDeadlockedThread(Thread* me) {
  // User-space

  user::library::fmt::BufWriter writer{report_buf, kReportBufSize};

  writer.FormatAppend("Thread #{} is blocked at {} ({})", me->id, me->waiter->operation, me->waiter->source_loc);
  if (me->waiter->waiting_for) {
    writer.FormatAppend(", waiting for thread #{}", *(me->waiter->waiting_for));
  }
  writer.FormatAppend("\n");

  // Stack trace

  /*
  std::ostringstream trace_out;
  PrintStackTrace(trace_out);
  const auto trace = trace_out.str();
  if (!trace.empty()) {
    report_out << trace << std::endl;
  }
  */

  me->report = writer.StringView();

  me->context.SwitchTo(loop_context_);  // Never returns

  WHEELS_UNREACHABLE();
}

void Simulator::SpuriousWakeups() {
  // TODO
}

bool Simulator::DeterministicMemory() const {
#if (__TWIST_FIBERS_ISOLATE_USER_MEMORY__)
  return memory_.FixedMapping();
#else
  return false;
#endif
}

bool Simulator::DigestAtomicLoads() const {
  if (params_.digest_atomic_loads) {
    return true;  // Explicit directive
  }

  // Conservative decision
  return params_.allow_digest_atomic_loads &&
         DeterministicMemory();
}

void Breakpoint(Thread* thread) {
  (void)thread;
}

void Simulator::DebugStart(Thread* thread) {
  Breakpoint(thread);
}

void Simulator::DebugInterrupt(Thread* thread) {
  Breakpoint(thread);
}

void Simulator::DebugSwitch(Thread* thread) {
  Breakpoint(thread);
}

void Simulator::DebugResume(Thread* thread) {
  Breakpoint(thread);
}


SystemAllocator* SystemAllocator::Get() {
  return Simulator::Current()->ObjectAllocator();
}

size_t ThreadCount() {
  return Simulator::Current()->ThreadCount();
}

}  // namespace system

}  // namespace twist::rt::fiber
