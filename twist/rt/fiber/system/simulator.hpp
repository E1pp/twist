#pragma once

#include <twist/rt/fiber/system/params.hpp>
#include <twist/rt/fiber/system/main.hpp>
#include <twist/rt/fiber/system/loop_budget.hpp>
#include <twist/rt/fiber/system/thread/struct.hpp>
#include <twist/rt/fiber/system/thread/trampoline.hpp>
#include <twist/rt/fiber/system/digest.hpp>
#include <twist/rt/fiber/system/time.hpp>
#include <twist/rt/fiber/system/futex/waiter.hpp>
#include <twist/rt/fiber/system/futex/wait_queue.hpp>
#include <twist/rt/fiber/system/timer/timer_queue.hpp>
#include <twist/rt/fiber/system/id_allocator.hpp>
#include <twist/rt/fiber/system/result.hpp>
#include <twist/rt/fiber/system/call/handler.hpp>
#include <twist/rt/fiber/system/call/status.hpp>
#include <twist/rt/fiber/system/wake_type.hpp>
#include <twist/rt/fiber/system/memory.hpp>
#include <twist/rt/fiber/system/logger.hpp>
#include <twist/rt/fiber/system/scheduler/scheduler.hpp>
#include <twist/rt/fiber/system/resource_manager.hpp>
#include <twist/rt/fiber/system/status.hpp>

#include <twist/rt/fiber/system/sync/action.hpp>
#include <twist/rt/fiber/system/sync/model.hpp>

#include <sure/context.hpp>

#include <wheels/core/assert.hpp>

#include <chrono>
#include <optional>
#include <string_view>
#include <sstream>
#include <random>

namespace twist::rt::fiber {

namespace system {

class Simulator
    : private IThreadUserState {
  friend void Trampoline(Thread*);

  friend struct SleepTimer;
  friend struct FutexTimer;

 public:
  using Params = system::Params;
  using Result = system::Result;
  using Status = system::Status;
  using Digest = uint64_t;

 public:
  Simulator(scheduler::IScheduler* scheduler, Params params = Params(),
            ResourceManager* resource_manager = nullptr);

  scheduler::IScheduler* Scheduler() const  {
    return scheduler_;
  }

  // One-shot
  Result Run(MainRoutine);

  // Debugger mode
  void Start(MainRoutine);
  size_t RunFor(size_t iters);
  bool RunOneIter();
  void Drain();
  Result Burn();

  static Simulator* Current() {
    WHEELS_VERIFY(current, "Not in simulator");
    return current;
  }

  // TODO: Workaround, remove later
  static Simulator* TryCurrent() {
    return current;
  }

  class SystemAllocator* ObjectAllocator() {
    return resource_manager_;
  }

  // System calls (Sys{Name})

  // To measure overhead
  void SysNoop();

  ThreadId SysSpawn(IThreadUserState*);
  ThreadId SysGetId();
  void SysDetach(ThreadId);  // Workaround for check

  void SysYield();

  call::Status SysSleepFor(Time::Duration);
  call::Status SysSleepUntil(Time::Instant);

  uint64_t SysSync(sync::Action* action);
  void SysAccessNonAtomicVar(sync::Access access);

  // Futex
  call::Status SysFutexWait(void* var, uint64_t old, const WaiterContext*);
  call::Status SysFutexWaitTimed(void* var, uint64_t old, Time::Instant, const WaiterContext*);
  void SysFutexWake(void* var, size_t);

  Time::Instant SysNow() const {
    // Optimize SystemCall: Allocation-free
    return time_.Now();
  }

  uint64_t SysRandomNumber();
  size_t SysRandomChoice(size_t alts);

  bool SysSpuriousTryFailure();

  void SysLog(wheels::SourceLocation loc, std::string_view message);

  void SysWrite(int fd, std::string_view buf);

  [[noreturn]] void SysAbort(Status);

  // Hints for scheduler
  void SysSchedHintLockFree(bool flag);
  void SysSchedHintProgress();
  void SysSchedHintNewIter();

  void SysThreadEnter();
  [[noreturn]] void SysThreadExit();

  bool AmIUser() const {
    return running_ != nullptr;
  }

  bool ForceGlobalAllocator() const {
    // Exceptions
    return stacktrace_;
  }

  bool UserMemoryAllocator() const {
    return AmIUser() && !ForceGlobalAllocator();
  }

  void* UserMalloc(size_t size) {
    WHEELS_ASSERT(AmIUser(), "Not in user-space");
    return memory_.Malloc(size);
  }

  void UserFree(void* ptr) {
    WHEELS_ASSERT(AmIUser(), "Not in user-space");
    memory_.Free(ptr);
  }

  void* UserAllocStatic(size_t size) {
    WHEELS_ASSERT(AmIUser(), "Not in user-space");
    return memory_.AllocateStatic(size);
  }

  void UserDeallocStatics() {
    WHEELS_ASSERT(AmIUser(), "Not in user-space");
    memory_.DeallocateStatics();
  }

  bool UserMemoryAccess(void* addr, size_t size) {
    return memory_.Access(addr, size);
  }

  Thread* RunningThread() const {
    WHEELS_ASSERT(AmIUser(), "Not in user-space");
    return running_;
  }

  user::tls::Storage& UserTls() {
    WHEELS_ASSERT(AmIUser(), "Not in user-space");
    return RunningThread()->tls;
  }

  bool Preemptive(bool on);

  void Silent(bool on) {
    silent_ = on;
  }

  bool Print(bool on) {
    return std::exchange(print_, on);
  }

  void Debug(bool on) {
    debug_ = on;
  }

  // Statistics for tests
  size_t IterCount() const {
    return iter_;
  }

  size_t ThreadCount() const {
    return thread_count_;
  }

  // Statistics for tests
  size_t FutexCount() const {
    return futex_wait_count_ + futex_wake_count_;
  }

  void Checkpoint() {
    last_checkpoint_ = iter_;
  }

  void DigestAtomicLoad(sync::ActionType action, uint64_t repr);

  // Scheduler context

  uint64_t CurrTick() const {
    return ticker_.Count();
  }

  size_t CurrIter() const {
    return iter_;
  }

  // From kernel
  void Prune(std::string why) {
    stderr_ << "Simulation aborted by scheduler: " << why << std::endl;
    _SysAbort(Status::Pruned);
  }

  sync::SeqCstMemoryModel& SharedMemory() {
    return memory_model_;
  }

 private:
  void ValidateParams();

  void Init();

  void SetCurrentScheduler();
  void ResetCurrentScheduler();

  // IThreadUserState
  void RunUser() override;
  void AtThreadExit() noexcept override;

  void SpawnMainThread(MainRoutine);
  void MainCompleted();

  void StartLoop(MainRoutine);
  // Return number of iterations made
  size_t RunLoop(LoopBudget);
  void StopLoop();

  scheduler::IRunQueue* RunQueue() const;

  void Iter();
  void Maintenance();
  bool Idle();

  void SpuriousWakeups();

  bool IsDeadlock() const;
  void CheckDeadlock();
  void ReportFromDeadlockedThread(Thread*);
  void ReportDeadlock();

  void CheckMemory();

  void AddTimer(Timer*);
  void CancelTimer(Timer*);
  // true means progress
  bool PollTimers();

  // Advance virtual time
  void Tick();

  // Debugging
  void DebugStart(Thread*);
  void DebugInterrupt(Thread*);
  void DebugSwitch(Thread*);
  void DebugResume(Thread*);

  // Context switches

  // Scheduler -> thread
  void SwitchTo(Thread*);
  // Running thread -> scheduler
  call::Status SystemCall(call::Handler handler);

  // Context switch: scheduler -> thread
  void Run(Thread*);
  void CompleteSync(Thread*);
  void Iter(Thread*);
  // true - resume immediately, false - suspend
  bool HandleSystemCall(Thread*);

  Thread* CreateThread(IThreadUserState* state);
  void DestroyThread(Thread*);

  void Burn(Thread*);
  void BurnDetachedThreads();

  // Add thread to run queue
  void ScheduleWaiter(Thread*, call::Status status = call::Status::Ok);
  void Wake(Thread* waker, Thread* waiter, WakeType);

  // Decision
  bool DigestAtomicLoads() const;
  bool DeterministicMemory() const;

  // System call handlers

  // NOLINTBEGIN
  ThreadId _SysSpawn(Thread* parent, IThreadUserState*);
  void _SysDetach(ThreadId id);
  bool _SysYield(Thread*);
  void _SysSleep(Thread*, Timer*);
  bool _SysSync(Thread*, sync::Action*);
  void _SysAccessNonAtomicVar(Thread*, sync::Access);
  void _SysFutexWait(Thread*, void* var, uint64_t old, Timer*, const WaiterContext*);
  void _SysFutexWake(Thread*, void* var, size_t);
  void _SysLog(Thread*, wheels::SourceLocation loc, std::string_view message);
  void _SysWrite(int fd, std::string_view buf);
  void _SysAbort(Status);
  void _SysThreadEnter(Thread*);
  void _SysThreadExit(Thread*);
  // NOLINTEND

  //

  uint64_t Sync(Thread*, sync::Action*);

  void PrintPrevThreadStatus(ThreadId);
  void PrintIter(Thread*);
  void PrintSync(Thread*, sync::Action*);

 private:
  const Params params_;

  scheduler::IScheduler* scheduler_;
  std::optional<ThreadId> last_thread_;

  ResourceManager* resource_manager_;

  Logger logger_;

  Ticker ticker_;
  Time time_;

  Memory memory_;
  // Workaround for backward-cpp
  bool stacktrace_{false};

  IdAllocator ids_;

  DigestCalculator digest_;
  // Cached
  bool digest_atomic_loads_{false};

  std::optional<MainRoutine> main_;

  wheels::IntrusiveList<Thread, AliveTag> alive_;

  size_t burnt_threads_ = 0;

  sure::ExecutionContext loop_context_;  // Thread context
  size_t iter_{0};
  // For debugger
  size_t last_checkpoint_{0};

  // Run queue
  // IRunQueue* RunQueue();
  Thread* running_{nullptr};
  std::optional<ThreadId> prev_thread_id_;

  sync::SeqCstMemoryModel memory_model_;

  // Timer queue
  TimerQueue timer_queue_;

  call::Handler syscall_;

  bool done_{false};

  std::stringstream stdout_;
  std::stringstream stderr_;
  std::optional<Status> status_;

  // For logging
  bool silent_{false};
  bool print_{false};
  bool debug_{false};

  // Statistics
  size_t futex_wait_count_{0};
  size_t futex_wake_count_{0};

  size_t thread_count_{0};

  static Simulator* current;
};

}  // namespace system

}  // namespace twist::rt::fiber
