#pragma once

#include <wheels/core/source_location.hpp>
#include <string>

#include <fmt/core.h>

#include <iostream>

namespace twist::rt::fiber {

namespace system {

struct LogEvent {
  wheels::SourceLocation source_loc;
  ThreadId id;
  std::string message;
};

class Logger {
 public:
   void Log(LogEvent e) {
     std::cout << Format(e) << std::endl;
   }

  private:
   std::string Format(const LogEvent& e) {
     return ::fmt::format("{} - {}[Line {}] - {}", e.id, e.source_loc.Function(), e.source_loc.Line(), e.message);
   }
};

}  // namespace system

}  // namespace twist::rt::fiber