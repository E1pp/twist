#include "struct.hpp"

#include "../simulator.hpp"

#include <twist/rt/fiber/user/library/fmt/print.hpp>

#include <twist/rt/fiber/user/static/manager.hpp>
#include <twist/rt/fiber/user/static/thread_local/manager.hpp>

namespace twist::rt::fiber {

namespace system {

void Trampoline(Thread* me) {
  Simulator* simulator = me->simulator;

  simulator->SysThreadEnter();

  if (me->main) {
    // Static global variables
    user::ss::Manager::Instance().ConstructGlobalVars();
  }

  try {
    me->user->RunUser();
  } catch (...) {
    user::library::fmt::Println(2, "Unhandled exception in thread #{}", me->id);
    simulator->SysAbort(Status::UnhandledException);  // Never returns
  }

  // Thread-local variables
  user::tls::Manager::Instance().Destroy(me->tls);

  me->user->AtThreadExit();

  // Static variables
  if (me->main) {
    user::ss::Manager::Instance().DestroyVars();
  }

  simulator->SysThreadExit();  // Never returns

  WHEELS_UNREACHABLE();
}

}  // namespace system

}  // namespace twist::rt::fiber
