#pragma once

#include <function2/function2.hpp>

namespace twist::rt::fiber {

namespace system {

struct IThreadRoutine {
  virtual ~IThreadRoutine() = default;

  virtual void RunUser() = 0;
};

struct IThreadExitHandler {
  virtual ~IThreadExitHandler() = default;

  virtual void AtThreadExit() noexcept = 0;
};

struct IThreadUserState : IThreadRoutine,
                          IThreadExitHandler {
};

}  // namespace system

}  // namespace twist::rt::fiber
