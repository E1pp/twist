#pragma once

namespace twist::rt::fiber {

namespace system {

enum class ThreadState {
  Starting,  // Initial state
  Runnable,  // In Scheduler run queue
  Running,
  Sleeping,
  Parked,
  Terminated,
  Deadlocked,
};

}  // namespace system

}  // namespace twist::rt::fiber
