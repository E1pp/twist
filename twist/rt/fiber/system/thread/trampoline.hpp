#pragma once

#include "../fwd.hpp"

namespace twist::rt::fiber {

namespace system {

void Trampoline(Thread*);

}  // namespace system

}  // namespace twist::rt::fiber
