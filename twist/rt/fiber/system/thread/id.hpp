#pragma once

#include <cstdint>

namespace twist::rt::fiber {

namespace system {

using ThreadId = uint8_t;

extern const ThreadId kImpossibleThreadId;

}  // namespace system

}  // namespace twist::rt::fiber
