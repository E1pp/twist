#include "../id_allocator.hpp"

namespace twist::rt::fiber {

namespace system {

const ThreadId kImpossibleThreadId = IdAllocator::kImpossibleId;

}  // namespace system

}  // namespace twist::rt::fiber
