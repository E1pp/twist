#include "resource_manager.hpp"

namespace twist::rt::fiber {

namespace system {

void ResourceManager::Warmup() {
  // No-op
}

ResourceManager::~ResourceManager() {
}

static thread_local ResourceManager static_manager;

ResourceManager* StaticResourceManager() {
  return &static_manager;
}

}  // namespace system

}  // namespace twist::rt::fiber
