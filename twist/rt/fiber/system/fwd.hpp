#pragma once

namespace twist::rt::fiber {

namespace system {

struct Thread;
class Simulator;

}  // namespace system

}  // namespace twist::rt::fiber
