#pragma once

#include "atomic.hpp"
#include "non_atomic.hpp"

#include "../thread/struct.hpp"
#include "../thread_count.hpp"

#include <map>

namespace twist::rt::fiber {

namespace system::sync {

// A Promising Semantics for Relaxed-Memory Concurrency
// https://sf.snu.ac.kr/publications/promising.pdf

class SeqCstMemoryModel {
 public:
  SeqCstMemoryModel() {
  }

  void Reset() {
    atomics_.clear();
    non_atomics_.clear();
  }

  // Threads

  void Tick(Thread* thread) {
    thread->sync.clock.current.Tick(thread->id);
    thread->sync.clock.epoch += 1;
  }

  void SpawnFiber(Thread* parent, Thread* child) {
    child->sync.clock.current.Assign(parent->sync.clock.current);
    Tick(child);
  }

  void WakeFiber(Thread* waker, Thread* waiter) {
    // Wake acts as release operation
    waiter->sync.clock.current.Join(waker->sync.clock.release);
    Tick(waiter);
  }

  // Sync

  uint64_t Sync(Thread* thread, Action* action) {
    Tick(thread);

    switch (action->type) {
      case ActionType::AtomicInit:
        AtomicInit(thread, action);
        return 0;
      case ActionType::AtomicDestroy:
        AtomicDestroy(thread, action);
        return 0;
      case ActionType::AtomicLoad:
        return AtomicLoad(thread, action);
      case ActionType::AtomicDebugLoad:
        return AtomicDebugLoad(thread, action);
      case ActionType::AtomicStore:
        AtomicStore(thread, action);
        return 0;
      case ActionType::AtomicRmwLoad:
        return AtomicRmwLoad(thread, action);
      case ActionType::AtomicRmwCommit:
        AtomicRmwCommit(thread, action);
        return 0;
      case ActionType::AtomicThreadFence:
        AtomicThreadFence(thread, action);
        return 0;
      default:
        WHEELS_PANIC("Unhandled synchronization action");
    }
  }

  // Futex

  uint64_t FutexLoad(Thread* /*thread*/, AtomicVar* atomic) {
    return atomic->last_store.value;
  }

  AtomicVar* FutexAtomic(void* loc) {
    return GetAtomic(loc);
  }

  AtomicVar* TryFutexAtomic(void* loc) {
    return TryGetAtomic(loc);
  }

  // Non-atomics

  std::optional<OldAccess> AccessNonAtomicVar(Thread* thread, Access access) {
    Tick(thread);

    NonAtomicVar* var;
    if (access.type == AccessType::Init) {
      auto [it, ok] = non_atomics_.try_emplace(access.loc);
      var = &it->second;

      var->Init();
      var->source_loc = access.source_loc;
    } else {
      auto it = non_atomics_.find(access.loc);
      if (it != non_atomics_.end()) {
        var = &it->second;
      } else {
        var = nullptr;
      }
      WHEELS_VERIFY(var != nullptr, "Non-atomic var not found");
    }

    if (auto race = CheckDataRaces(thread, var, access)) {
      return race;
    }

    if (access.type == AccessType::Destroy) {
      non_atomics_.erase(access.loc);
    } else {
      var->Log({
          access.loc,
          access.type,
          thread->id,
          thread->sync.clock.epoch,
          access.source_loc});
    }

    return std::nullopt;
  }

 private:
  // Atomics

  AtomicVar* TryGetAtomic(void* loc) {
    auto it = atomics_.find(loc);
    if (it != atomics_.end()) {
      return &it->second;
    } else {
      return nullptr;
    }
  }

  AtomicVar* GetAtomic(void* loc) {
    AtomicVar* a = TryGetAtomic(loc);
    WHEELS_VERIFY(a != nullptr, "Atomic not found");
    return a;
  }

  void AtomicInit(Thread*, Action* init) {
    auto [it, ok] = atomics_.try_emplace(init->loc);
    AtomicVar* atomic = &it->second;

    atomic->source_loc = init->source_loc;
    atomic->last_store.value = init->value;
    atomic->last_store.clock.Init();  // Initialization is not atomic
    atomic->futex.Init();
  }

  uint64_t AtomicLoad(Thread* reader, Action* load) {
    WHEELS_ASSERT(IsLoadOrder(load->mo), "Unexpected memory order for atomic load");

    AtomicVar* atomic = GetAtomic(load->loc);

    // Acquire load ~ Relaxed load ; Acquire fence
    uint64_t value = AtomicLoadRelaxed(reader, atomic);
    if (IsAcquireOrder(load->mo)) {
      AtomicThreadFenceAcquire(reader);
    }

    return value;
  }

  uint64_t AtomicDebugLoad(Thread* /*reader*/, Action* load) {
    AtomicVar* atomic = GetAtomic(load->loc);
    return atomic->last_store.value;
  }

  void AtomicStore(Thread* writer, Action* store) {
    WHEELS_ASSERT(IsStoreOrder(store->mo), "Unexpected memory order for atomic store");

    AtomicVar* atomic = GetAtomic(store->loc);

    // Atomic store ~ Release fence ; Relaxed store
    if (IsReleaseOrder(store->mo)) {
      AtomicThreadFenceRelease(writer);
    }
    AtomicStoreRelaxed(writer, atomic, store->value);
  }

  uint64_t AtomicRmwLoad(Thread* writer, Action* rmw) {
    AtomicVar* atomic = GetAtomic(rmw->loc);

    uint64_t value = AtomicLoadRelaxed(writer, atomic);
    if (IsAcquireOrder(rmw->mo)) {
      AtomicThreadFenceAcquire(writer);
    }
    return value;
  }

  void AtomicRmwCommit(Thread* writer, Action* rmw) {
    AtomicVar* atomic = GetAtomic(rmw->loc);

    if (IsReleaseOrder(rmw->mo)) {
      AtomicThreadFenceRelease(writer);
    }
    AtomicRmwCommitRelaxed(writer, atomic, rmw->value);
  }

  void AtomicDestroy(Thread*, Action* destroy) {
    AtomicVar* atomic = GetAtomic(destroy->loc);
    WHEELS_VERIFY(atomic->futex.IsEmpty(), "Futex queue is not empty");
    atomics_.erase(destroy->loc);
  }

  static uint64_t AtomicLoadRelaxed(Thread* reader, AtomicVar* atomic) {
    reader->sync.clock.acquire.Join(atomic->last_store.clock);
    return atomic->last_store.value;
  }

  static void AtomicStoreRelaxed(Thread* writer, AtomicVar* atomic, uint64_t value) {
    atomic->last_store.value = value;
    atomic->last_store.clock.Assign(writer->sync.clock.release);
  }

  static void AtomicRmwCommitRelaxed(Thread* writer, AtomicVar* atomic, uint64_t value) {
    atomic->last_store.clock.Join(writer->sync.clock.release);
    atomic->last_store.value = value;
  }

  // Fences

  static void AtomicThreadFenceRelease(Thread* thread) {
    // release < current
    thread->sync.clock.release.Assign(thread->sync.clock.current);
  }

  static void AtomicThreadFenceAcquire(Thread* thread) {
    thread->sync.clock.current.Join(thread->sync.clock.acquire);
  }

  static void AtomicThreadFenceAcqRel(Thread* thread) {
    AtomicThreadFenceAcquire(thread);
    AtomicThreadFenceRelease(thread);
  }

  static void AtomicThreadFence(Thread* thread, Action* fence) {
    switch (fence->mo) {
      case std::memory_order::acquire:
        AtomicThreadFenceAcquire(thread);
        return;
      case std::memory_order::release:
        AtomicThreadFenceRelease(thread);
        return;
      case std::memory_order::acq_rel:
        AtomicThreadFenceAcqRel(thread);
        return;
      default:
        WHEELS_PANIC("Fence not supported");
    }
  }

 private:
  // Non-atomics

  static bool Conflict(AccessType a, AccessType b) {
    return IsWrite(a) || IsWrite(b);
  }

  static bool HappensBefore(const OldAccess& old, const Thread* thread) {
    return thread->sync.clock.current.Get(old.thread) >= old.epoch;
  }

  std::optional<OldAccess> CheckDataRaces(const Thread* thread, NonAtomicVar* var, const Access& curr) {
    if (var->last_write && (var->last_write->thread != thread->id)) {
      // Compare current access with the last write (from different thread)
      OldAccess& last_write = *(var->last_write);
      if (!HappensBefore(last_write, thread)) {
        return last_write;
      }
    }

    if (IsWrite(curr.type)) {
      // Compare current write with the last read from each thread

      size_t n = ThreadCount();
      for (size_t i = 1; i <= n; ++i) {
        if ((i != thread->id) && (var->last_read[i])) {
          OldAccess& old = *(var->last_read[i]);
          if (!HappensBefore(old, thread)) {
            return old;
          }
        }
      }
    }

    return std::nullopt;
  }

 private:
  std::map<void*, AtomicVar> atomics_;
  std::map<void*, NonAtomicVar> non_atomics_;
};

}  // namespace system::sync

}  // namespace twist::rt::fiber
