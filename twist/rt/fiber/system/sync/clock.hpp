#pragma once

#include "../limits.hpp"
#include "../thread_count.hpp"

// https://github.com/llvm/llvm-project/blob/main/compiler-rt/lib/tsan/rtl/tsan_vector_clock.cpp

#if defined(__TWIST_FIBERS_VECTORIZE_CLOCKS__)
#include <immintrin.h>
#else
#include <algorithm>
#endif

#include <wheels/core/assert.hpp>

#include <cstdlib>
#include <cstring>

namespace twist::rt::fiber {

namespace system::sync {

using Epoch = uint32_t;

class VectorClock {
  static const size_t kClockSize = kMaxThreads;

#if defined(__TWIST_FIBERS_VECTORIZE_CLOCKS__)
  static const size_t kVectorizedClockSize = (kClockSize * sizeof(Epoch)) / 16;
#endif

 public:
  void Init() {
    std::memset(v_, 0, kClockSize * sizeof(Epoch));
  }

  Epoch Get(size_t tid) const {
    return v_[tid - 1];
  }

  Epoch Tick(size_t tid) {
    WHEELS_ASSERT(tid <= ThreadCount(), "Too many threads");
    return ++v_[tid - 1];
  }

  void Reset() {
#if !defined(__TWIST_FIBERS_VECTORIZE_CLOCKS__)
    size_t n = ThreadCount();
    for (size_t i = 0; i < n; ++i) {
      v_[i] = 0;
    }
#else
    __m128i z = _mm_setzero_si128();
    __m128i* vv = reinterpret_cast<__m128i*>(v_);
    for (size_t i = 0; i < kVectorizedClockSize; ++i) {
      _mm_store_si128(&vv[i], z);
    }
#endif
  }

  void Assign(const VectorClock& that) {
#if !defined(__TWIST_FIBERS_VECTORIZE_CLOCKS__)
    size_t n = ThreadCount();
    for (size_t i = 0; i < n; ++i) {
      v_[i] = that.v_[i];
    }
#else
    __m128i* __restrict vv = reinterpret_cast<__m128i*>(v_);
    __m128i const* __restrict that_vv = reinterpret_cast<__m128i const*>(that.v_);
    for (size_t i = 0; i < kVectorizedClockSize; ++i) {
      __m128i s = _mm_load_si128(&that_vv[i]);
      _mm_store_si128(&vv[i], s);
    }
#endif
  }

  void Join(const VectorClock& with) {
#if !defined(__TWIST_FIBERS_VECTORIZE_CLOCKS__)
    size_t n = ThreadCount();
    for (size_t i = 0; i < n; ++i) {
      v_[i] = std::max(v_[i], with.v_[i]);
    }
#else
    __m128i* __restrict vv = reinterpret_cast<__m128i*>(v_);
    __m128i const * __restrict with_vv = reinterpret_cast<__m128i const*>(with.v_);

    for (size_t i = 0; i < kVectorizedClockSize; ++i) {
      __m128i s = _mm_load_si128(&with_vv[i]);
      __m128i d = _mm_load_si128(&vv[i]);
      __m128i m = _mm_max_epu32(s, d);
      _mm_store_si128(&vv[i], m);
    }
#endif
  }

 private:
  Epoch v_[kClockSize];
};

}  // namespace system::sync

}  // namespace twist::rt::fiber
