#pragma once

#include <twist/rt/fiber/system/scheduler.hpp>

#include "params.hpp"
#include "thread_set.hpp"

#include <twist/wheels/random/wyrand.hpp>

#include <wheels/core/assert.hpp>
#include <wheels/core/hash.hpp>

namespace twist::rt::fiber {

namespace system::scheduler::random {

class Scheduler final : public IScheduler {
 public:
  using Params = random::Params;

 private:
  // Futex queue

  class WaitQueue : public IWaitQueue {
   public:
    WaitQueue(Scheduler* host)
        : host_(host) {
    }

    bool IsEmpty() const override {
      return set_.IsEmpty();
    }

    void Push(Thread* f) override {
      set_.Push(f);
    }

    Thread* Pop() override {
      return set_.PopRandom(host_->RandomNumber());
    }

    Thread* PopAll() override {
      return set_.PopAll();
    }

    bool Remove(Thread* f) override {
      return set_.Remove(f);
    }

   private:
    Scheduler* host_;
    ThreadSet set_;
  };

 public:
  Scheduler(Params params = Params())
      : params_(params),
        seed_(params_.seed),
        random_(seed_) {
  }

  bool NextSchedule() override {
    if (params_.max_runs && ++runs_ > *params_.max_runs) {
      return false;
    }

    wheels::HashCombine(seed_, runs_);  // Next seed
    return true;
  }

  void Start(Simulator* simulator) override {
    simulator_ = simulator;

    random_.Seed(seed_);
  }

  void Interrupt(Thread* active) override {
    active_ = active;
  }

  void Yield(Thread* active) override {
    run_queue_.Push(active);
  }

  void Wake(Thread* waiter) override {
    run_queue_.Push(waiter);
  }

  void Spawn(Thread* thread) override {
    run_queue_.Push(thread);
  }

  void Exit(Thread*) override {
    // No-op
  }

  // Hints

  void LockFree(Thread* /*thread*/, bool /*flag*/) override {
    // No-op
  }

  void Progress(Thread* /*thread*/) override {
    // No-op
  }

  void NewIter() override {
    // No-op
  }

  // Run queue

  bool IsIdle() const override {
    return run_queue_.IsEmpty() && (active_ == nullptr);
  }

  Thread* PickNext() override {
    return PickNext(std::exchange(active_, nullptr));
  }

  void Remove(Thread* thread) override {
    run_queue_.Remove(thread);
  }

  // Futex

  IWaitQueuePtr NewWaitQueue() override {
    return std::make_unique<WaitQueue>(this);
  }

  // Random

  uint64_t RandomNumber() override {
    return random_.Next();
  }

  size_t RandomChoice(size_t alts) override {
    return random_.Next() % alts;
  }

  // Spurious

  bool SpuriousWakeup() override {
    return Spurious(params_.spurious_wakeups);
  }

  bool SpuriousTryFailure() override {
    return Spurious(params_.spurious_failures);
  }

 private:
  Thread* PickNext(Thread* active) {
    if (active != nullptr) {
      if (Preempt(active)) {
        run_queue_.Push(active);
      } else {
        return active;
      }
    }

    Thread* next = run_queue_.PopRandom(random_.Next());
    next->sched.f1 = simulator_->CurrTick() + RandomTimeSliceInTicks();
    return next;
  }

  bool Preempt(Thread* active) {
    if (!active->preemptive) {
      return false;
    }

    return simulator_->CurrTick() >= active->sched.f1;
  }

  size_t RandomTimeSliceInTicks() {
    return random_.Next() % params_.time_slice;
  }

  bool Spurious(Rational p) {
    return (random_.Next() % p.denom) < p.num;
  }

 private:
  const Params params_;

  size_t seed_;
  twist::random::WyRand random_;
  size_t runs_ = 0;

  Simulator* simulator_;

  ThreadSet run_queue_;
  Thread* active_ = nullptr;
};

}  // namespace system::scheduler::random

}  // namespace twist::rt::fiber
