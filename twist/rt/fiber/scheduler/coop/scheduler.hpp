#pragma once

#include <twist/rt/fiber/system/scheduler.hpp>

#include "params.hpp"

#include <twist/rt/fiber/system/simulator.hpp>

#include <twist/wheels/random/wyrand.hpp>

#include <wheels/core/assert.hpp>
#include <wheels/intrusive/list.hpp>

namespace twist::rt::fiber {

namespace system::scheduler::coop {

class Scheduler final : public IScheduler {
 public:
  using Params = coop::Params;

 private:
  // Futex queue

  class WaitQueue : public IWaitQueue {
   public:
    bool IsEmpty() const override {
      return waiters_.IsEmpty();
    }

    void Push(Thread* waiter) override {
      return waiters_.PushBack(waiter);
    }

    Thread* Pop() override {
      return waiters_.PopFront();
    }

    Thread* PopAll() override {
      return Pop();
    }

    bool Remove(Thread* thread) override {
      if (waiters_.IsLinked(thread)) {
        waiters_.Unlink(thread);
        return true;
      } else {
        return false;
      }
    }

   private:
    wheels::IntrusiveList<Thread, SchedulerTag> waiters_;
  };

 public:
  Scheduler(Params params = Params())
      : params_(params),
        random_(params_.seed) {
    WHEELS_UNUSED(params);
  }

  bool NextSchedule() override {
    return false;
  }

  ~Scheduler() {
    WHEELS_VERIFY(run_queue_.IsEmpty(), "Non-empty run queue");
  }

  void Start(Simulator*) override {
    // No-op
  }

  // System

  void Interrupt(Thread* active) override {
    run_queue_.PushFront(active);
  }

  void Yield(Thread* active) override {
    run_queue_.PushBack(active);
  }

  void Wake(Thread* waiter) override {
    run_queue_.PushBack(waiter);
  }

  void Spawn(Thread* thread) override {
    run_queue_.PushBack(thread);
  }

  void Exit(Thread*) override {
    // No-op
  }

  // Run queue

  bool IsIdle() const override {
    return run_queue_.IsEmpty();
  }

  Thread* PickNext() override {
    return run_queue_.PopFront();
  }

  void Remove(Thread* thread) override {
    if (run_queue_.IsLinked(thread)) {
      run_queue_.Unlink(thread);
    }
  }

  // Hints

  void LockFree(Thread* /*thread*/, bool /*flag*/) override {
    // No-op
  }

  void Progress(Thread* /*thread*/) override {
    // No-op
  }

  void NewIter() override {
    // No-op
  }

  // Futex

  IWaitQueuePtr NewWaitQueue() override {
    return std::make_unique<WaitQueue>();
  }

  // Random

  uint64_t RandomNumber() override {
    return random_.Next();
  }

  size_t RandomChoice(size_t alts) override {
    return random_.Next() % alts;
  }

  // Spurious

  bool SpuriousWakeup() override {
    return false;
  }

  bool SpuriousTryFailure() override {
    return false;
  }

 private:
  const Params params_;
  twist::random::WyRand random_;

  wheels::IntrusiveList<Thread, SchedulerTag> run_queue_;
};

}  // namespace system::scheduler::coop

}  // namespace twist::rt::fiber
