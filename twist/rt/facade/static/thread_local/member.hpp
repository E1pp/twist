#pragma once

#if defined(__TWIST_FIBERS__)

#include <twist/rt/fiber/user/static/thread_local/var.hpp>

#define TWISTED_STATIC_THREAD_LOCAL_MEMBER_DECLARE(M, name) \
  static twist::rt::fiber::user::StaticThreadLocalVar<M> name

#define TWISTED_STATIC_THREAD_LOCAL_MEMBER_DEFINE(T, M, name) \
  twist::rt::fiber::user::StaticThreadLocalVar<M> T::name{#name, /*member=*/true}

#else

#include <twist/rt/thread/static/thread_local/var.hpp>

#define TWISTED_STATIC_THREAD_LOCAL_MEMBER_DECLARE(M, name) \
  static thread_local twist::rt::thread::StaticThreadLocalVar<M> name

#define TWISTED_STATIC_THREAD_LOCAL_MEMBER_DEFINE(T, M, name) \
  thread_local twist::rt::thread::StaticThreadLocalVar<M> T::name{}

#endif
