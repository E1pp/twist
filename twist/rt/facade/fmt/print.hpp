#pragma once

#if defined(__TWIST_FIBERS__)

#include <twist/rt/fiber/user/library/fmt/print.hpp>

namespace twist::rt::facade {

namespace fmt {

using fiber::user::library::fmt::Print;
using fiber::user::library::fmt::Println;

}  // namespace fmt

}  // namespace twist::rt::facade

#else

#include <twist/rt/thread/fmt/print.hpp>

namespace twist::rt::facade {

namespace fmt {

using thread::fmt::Print;
using thread::fmt::Println;

}  // namespace fmt

}  // namespace twist::rt::facade

#endif
