#pragma once

#if defined(__TWIST_FIBERS__)

#include <twist/rt/fiber/user/library/mem/malloc.hpp>

namespace twist::rt::facade {

namespace mem {

using fiber::user::library::mem::Malloc;
using fiber::user::library::mem::Free;

}  // namespace mem

}  // namespace twist::rt::facade

#else

#include <twist/rt/thread/mem/malloc.hpp>

namespace twist::rt::facade {

namespace mem {

using thread::mem::Malloc;
using thread::mem::Free;

}  // namespace mem

}  // namespace twist::rt::facade

#endif
