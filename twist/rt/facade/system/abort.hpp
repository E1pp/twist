#pragma once

#if defined(__TWIST_FIBERS__)

#include <twist/rt/fiber/user/library/system/abort.hpp>

namespace twist::rt::facade {

namespace system {

using fiber::user::library::system::Abort;

}  // namespace system

}  // namespace twist::rt::facade

#else

#include <twist/rt/thread/system/abort.hpp>

namespace twist::rt::facade {

namespace system {

using thread::Abort;

}  // namespace system

}  // namespace twist::rt::facade

#endif
