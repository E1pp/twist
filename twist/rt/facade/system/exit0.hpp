#pragma once

#if defined(__TWIST_FIBERS__)

#include <twist/rt/fiber/user/library/system/exit0.hpp>

namespace twist::rt::facade {

namespace system {

using fiber::user::library::system::Exit0;

}  // namespace system

}  // namespace twist::rt::facade

#else

#include <twist/rt/thread/system/exit0.hpp>

namespace twist::rt::facade {

namespace system {

using thread::Exit0;

}  // namespace system

}  // namespace twist::rt::facade

#endif
