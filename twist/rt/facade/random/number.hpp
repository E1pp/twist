#pragma once

#include <limits>

#if defined(__TWIST_FIBERS__)

#include <twist/rt/fiber/user/library/random/number.hpp>

namespace twist::rt::facade {

namespace random {

// Number()
using fiber::user::library::random::Number;

}  // namespace random

}  // namespace twist::rt::facade

#else

#include <twist/rt/thread/random/number.hpp>

namespace twist::rt::facade {

namespace random {

// Number()
using thread::random::Number;

}  // namespace random

}  // namespace twist::rt::facade

#endif

namespace twist::rt::facade {

namespace random {

// [lo, hi]
inline uint64_t Number(uint64_t lo, uint64_t hi) {
  if (lo == 0 && hi == std::numeric_limits<uint64_t>::max()) {
    return Number();  // Full range
  } else {
    return lo + Number() % (hi - lo + 1);
  }
}

}  // namespace random

}  // namespace twist::rt::facade