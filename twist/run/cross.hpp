#pragma once

#include <twist/build.hpp>

#include <twist/run/cross/params.hpp>

#include <twist/run/main.hpp>

namespace twist::run {

// Standalone run
void Cross(cross::Params params, MainRoutine main);
void Cross(MainRoutine main);

}  // namespace twist::run
