#pragma once

#include <twist/run/sim/params.hpp>

namespace twist::run::cross {

struct Params {
  sim::Params sim;
};

}  // namespace twist::run::cross
