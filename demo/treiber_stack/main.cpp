#include "treiber_stack.hpp"

#include <twist/mod/sim.hpp>

#include <twist/test/wg.hpp>

#include <fmt/core.h>

static_assert(twist::build::IsolatedSim());

void TestCase() {
  LockFreeStack<int> stack;

  twist::test::WaitGroup{}
      .Add(/*threads=*/2, [&stack] {
        stack.Push(1);
        stack.TryPop();
      })
      .Join();
}

int main() {
  // twist::sim::AssignMemoryRange((void*)0x0200000000ULL, 4LL * 1024 * 1024 * 1024);

  twist::sim::sched::DfsScheduler dfs{{.max_preemptions = 5}};

  auto exp = twist::sim::Explore(dfs, TestCase);

  fmt::println("Simulations: {}", exp.sims);

  if (exp.found) {
    auto [schedule, result] = *exp.found;
    assert(result.status == twist::sim::Status::MemoryAccess);
    fmt::println("Stderr: {}", result.std_err);  // Corrupted memory report

    twist::sim::sched::Print(TestCase, schedule);
  } else {
    fmt::println("Ok");
  }

  return 0;
}
