#pragma once

#include <twist/ed/std/atomic.hpp>

#include <twist/ed/fmt/print.hpp>

#include <twist/assist/shared.hpp>
#include <twist/assist/memory.hpp>

// Treiber lock-free stack

template <typename T>
class LockFreeStack {
  struct Node {
    twist::assist::Shared<T> datum;
    Node* next;
  };

 public:
  void Push(T datum) {
    Node* new_top = new Node{std::move(datum), top_.load(std::memory_order::relaxed)};
    while (!top_.compare_exchange_weak(new_top->next, new_top, std::memory_order::release)) {
      // Try again
    }
  }

  std::optional<T> TryPop() {
    while (true) {
      Node* top = top_.load(std::memory_order::acquire);

      if (top == nullptr) {
        return std::nullopt;
      }

      Node* top_next = twist::assist::Ptr(top)->next;  // <- Memory check (heap-use-after-free)

      if (top_.compare_exchange_weak(top, top_next, std::memory_order::relaxed)) {
        T datum = std::move(*(twist::assist::Ptr(top)->datum));  // <- Memory check (heap-use-after-free)
        delete top;  // <- Automatic memory check (double-free)
        // Retire(top);
        return datum;
      }
    }
  }

  ~LockFreeStack() {
    while (TryPop()) {}
    Free();
  }

 private:
  void Retire(Node* node) {
    node->next = retire_top_.load(std::memory_order::relaxed);
    while (!retire_top_.compare_exchange_weak(node->next, node, std::memory_order::release)) {
      // Try again
    }
  }

  void Free() {
    Node* top = retire_top_.load(std::memory_order::relaxed);
    while (top != nullptr) {
      Node* next = top->next;
      delete top;
      top = next;
    }
  }


 private:
  twist::ed::std::atomic<Node*> top_{nullptr};
  twist::ed::std::atomic<Node*> retire_top_{nullptr};
};
