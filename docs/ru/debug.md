# Отладка

## Симуляция

```cpp
#include <twist/run/sim.hpp>

int main() {
  twist::run::SimParms params;
  params.seed = 42;
  
  twist::run::Sim(params, [] {
    /* Test routine */
  });
  
  return 0;
}
```

### Параметры симуляции

См. [`SimParams`](/twist/rt/layer/fiber/runtime/params.hpp)

- `seed` – для инициализации ГПСЧ
- `tick` – виртуальное время на шаг файбера
- `min_stack_size` – минимальный размер стека для файберов

### Отладка симуляции

```cpp
#include <twist/run/sim.hpp>

int main() {
  twist::run::SimParams params;
  params.seed = 42;
  
  twist::run::SimDebugger sim{params};
  
  sim.Start([] {
    /* Test routine */
  });
  
  // Пропускаем неинтересные шаги теста 
  sim.Skip(/*iters=*/128);
  
  // 0) Установите брейкпоинты в методах On{Event} у SimDebugger
  // 1) Установите брейкпоинт на вызове sim.Run() ниже
  // 2) Удачной отладки!
  sim.Run();

  return 0;
```

#### Брейкпоинты

Установите брейкпоинты [в методах `On{Event}`](/twist/run/sim/debugger.cpp):
так вы сможете наблюдать все переключения (и другие события), происходящие в симуляции.

Выше по стеку будет вызов, указывающий на тип события:
- переключение файбера (`OnSwitch`)
- возобновление файбера (`OnResume`)
- старт нового файбера (`OnStart`)

Каждый из этих вызовов получает аргументом указатель на файбер, с которым связано событие.

## Атомики

Атомики поддерживают чтение, не приводящее к fault injection:
- Метод `DebugLoad` у `atomic<T>`
- Метод `DebugTest` у `atomic_flag`

Используйте эти методы для отладочного логирования (в детерминированной симуляции).
