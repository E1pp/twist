#include <twist/run/cross.hpp>

#include <twist/ed/spin/lock.hpp>

#include <twist/test/wg.hpp>
#include <twist/test/plate.hpp>

using namespace std::chrono_literals;

void TestSpinLock() {
  twist::run::Cross([] {
    twist::test::Plate plate;
    twist::ed::SpinLock spinlock;

    twist::test::WaitGroup{}
        .Add(3, [&] {
          for (size_t i = 0; i < 65'536; ++i) {
            std::lock_guard guard{spinlock};
            plate.Access();
          }
        })
        .Join();
  });
}

int main() {
  TestSpinLock();
  return 0;
}
