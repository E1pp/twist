#include <twist/mod/sim.hpp>

#include <twist/ed/std/atomic.hpp>
#include <twist/ed/std/thread.hpp>

#include <chrono>
#include <cassert>

using namespace std::chrono_literals;

static_assert(twist::build::IsolatedSim());

int main() {
  {
    // yield

    twist::sim::sched::FairScheduler fair{};
    twist::sim::Simulator sim{&fair};

    auto result = sim.Run([] {
      twist::ed::std::atomic<size_t> iter{0};

      twist::ed::std::thread t([&] {
        while (true) {
          ++iter;
          twist::ed::std::this_thread::yield();
        }
      });

      t.detach();
      assert(!t.joinable());

      while (iter < 7) {
        twist::ed::std::this_thread::sleep_for(1ms);
      }

      // <- t is running
    });

    assert(result.Ok());
  }

  {
    // sleep_for

    twist::sim::sched::FairScheduler fair{};
    twist::sim::Simulator sim{&fair};

    auto result = sim.Run([] {
      twist::ed::std::atomic<size_t> iter{0};

      twist::ed::std::thread t([&] {
        while (true) {
          ++iter;
          twist::ed::std::this_thread::sleep_for(1s);
        }
      });

      t.detach();

      while (iter < 7) {
        twist::ed::std::this_thread::sleep_for(1s);
      }

      // <- t is running
    });

    assert(result.Ok());
  }

  return 0;
}
