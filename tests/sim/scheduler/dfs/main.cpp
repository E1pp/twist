#include <twist/mod/sim.hpp>

#include <twist/ed/std/atomic.hpp>
#include <twist/ed/std/mutex.hpp>
#include <twist/ed/std/thread.hpp>

#include <twist/ed/system/abort.hpp>

#include <twist/ed/random/choice.hpp>

#include <twist/ed/safety/assert.hpp>

#include <twist/assist/memory.hpp>
#include <twist/assist/preempt.hpp>
#include <twist/assist/prune.hpp>

#include <twist/test/wg.hpp>
#include <twist/test/either.hpp>

#include <fmt/core.h>

static_assert(twist::build::IsolatedSim());

struct ExploreResult {
  std::optional<twist::sim::Result> crash;
  size_t count = 0;
};

ExploreResult Explore(twist::sim::sched::DfsScheduler::Params dfs_params, std::function<void()> main) {
  ExploreResult result{};

  auto sim_params = twist::sim::SimulatorParams{};
  sim_params.forward_stdout = false;
  sim_params.crash_on_abort = false;
  sim_params.randomize_malloc = false;

  twist::sim::sched::DfsScheduler scheduler{dfs_params};
  do {
    twist::sim::Simulator sim{&scheduler, sim_params};

    ++result.count;
    auto sim_result = sim.Run([main] { main(); });

    if (!sim_result.Ok() && sim_result.Complete() && !result.crash) {
      result.crash = sim_result;
      // break;
    }
  } while (scheduler.NextSchedule());

  return result;
}

int main() {
  {
    // Deadlock
    auto params = twist::sim::sched::DfsScheduler::Params{};

    auto result = Explore(params, [] {
      twist::ed::std::mutex mu1;
      twist::ed::std::mutex mu2;

      twist::ed::std::thread t1([&] {
        mu1.lock();
        mu2.lock();
        mu2.unlock();
        mu1.unlock();
      });

      twist::ed::std::thread t2([&] {
        mu2.lock();
        mu1.lock();
        mu1.unlock();
        mu2.unlock();
      });

      t1.join();
      t2.join();
    });

    assert(result.crash);

    auto crash = *result.crash;
    assert(crash.status == twist::sim::Status::Deadlock);
    fmt::println("Deadlock stderr: {}", crash.std_err);
  }

  {
    // max_steps

    auto params = twist::sim::sched::DfsScheduler::Params{};
    params.max_steps = 17;

    auto result = Explore(params, [] {
      twist::ed::std::atomic<int> atom{0};

      while (true) {
        atom.fetch_add(1);
      }
    });

    assert(!result.crash);
    assert(result.count == 1);
  }

  {
    auto params = twist::sim::sched::DfsScheduler::Params{};

    // Terminates
    auto result = Explore(params, [] {
      twist::ed::std::atomic<bool> f{false};

      twist::ed::std::thread t([&f] {
        f.store(true);
      });

      while (!f.load()) {
        twist::assist::Prune("Ignore spin loops");
      }

      t.join();
    });

    assert(!result.crash);
  }

  {
    // max_preempts

    auto params = twist::sim::sched::DfsScheduler::Params{};
    params.max_preemptions = 1;

    auto result = Explore(params, [] {
      twist::ed::std::atomic<int> atom{0};

      auto incrs = [&atom] {
        for (size_t i = 0; i < 100; ++i) {
          atom.fetch_add(1);
        }
      };

      twist::ed::std::thread t1(incrs);
      twist::ed::std::thread t2(incrs);

      t1.join();
      t2.join();

      TWISTED_VERIFY(atom.load() == 200, "Missing increments");
    });

    assert(!result.crash);
    fmt::println("Incr simulations = {}", result.count);
  }

  {
    // random::Choose, random::Either

    auto params = twist::sim::sched::DfsScheduler::Params{};

    auto result = Explore(params, [] {
      twist::ed::std::atomic<size_t> atom{0};

      if (twist::ed::random::Either()) {
        atom.fetch_add(1);
      } else {
        atom.fetch_add(2);
      }

      auto r1 = twist::ed::random::Choose(2);
      auto r2 = twist::ed::random::Choose(3);
      auto r3 = twist::ed::random::Choose(4);

      WHEELS_UNUSED(r1 + r2 + r3);
    });

    assert(!result.crash);
    assert(result.count == 2 * 2 * 3 * 4);
  }

  {
    // SpinLock

    class SpinLock {
     public:
      void Lock() {
        while (!TryLock()) {
          // Try again
        }
      }

      bool TryLock() {
        return !locked_.exchange(true);
      }

      void Unlock() {
        locked_.store(false);
      }

     private:
      twist::ed::std::atomic<bool> locked_{false};
    };

    auto params = twist::sim::sched::DfsScheduler::Params{};
    params.max_steps = 15;
    params.force_preempt_after_steps = 5;
    params.max_preemptions = 3;

    auto result = Explore(params, [] {
      SpinLock spinlock;

      auto contender = [&] {
        while (true) {
          if (twist::test::Either()) {
            spinlock.Lock();
            spinlock.Unlock();
          } else {
            if (spinlock.TryLock()) {
              spinlock.Unlock();
            }
          }
        }
      };

      twist::ed::std::thread t1(contender);
      twist::ed::std::thread t2(contender);

      t1.join();
      t2.join();
    });

    fmt::println("SpinLock simulations: {}", result.count);
  }

  {
    // Interleaving 1

    auto params = twist::sim::sched::DfsScheduler::Params{};

    auto result = Explore(params, [] {
      twist::ed::std::atomic<int> atom{0};

      twist::test::WaitGroup{}
          .Add(1, [&] {
            atom.fetch_add(1);
            atom.fetch_add(1);
            atom.fetch_add(1);
          })
          .Join();
    });

    assert(result.count == 1);
  }

  {
    // Interleaving 2

    auto params = twist::sim::sched::DfsScheduler::Params{};

    auto result = Explore(params, [] {
      twist::ed::std::atomic<int> atom{0};

      twist::test::WaitGroup{}
          .Add(2, [&] {
            atom.fetch_add(1);
            atom.fetch_add(1);
            atom.fetch_add(1);
            atom.fetch_add(1);
          })
          .Join();
    });

    // https://math.stackexchange.com/questions/77721/number-of-instruction-interleaving
    assert(result.count == 252);
  }

  {
    // Interleaving 3

    auto params = twist::sim::sched::DfsScheduler::Params{};

    auto result = Explore(params, [] {
      twist::ed::std::atomic<int> atom{0};

      twist::test::WaitGroup{}
          .Add(3, [&] {
            atom.fetch_add(1);
            atom.fetch_add(1);
          })
          .Join();
    });

    // https://math.stackexchange.com/questions/77721/number-of-instruction-interleaving
    assert(result.count == 1680);
  }

  {
    // Interleaving 4

    auto params = twist::sim::sched::DfsScheduler::Params{};

    auto result = Explore(params, [] {
      twist::ed::std::atomic<int> atom{0};

      twist::test::WaitGroup{}
          .Add(2, [&] {
            twist::assist::PreemptionPoint();
          })
          .Join();
    });

    // 1 1 2 2
    // 1 2 1 2
    // 1 2 2 1
    // + symmetric
    assert(result.count == 6);
  }

  {
    // Interleaving 5

    auto params = twist::sim::sched::DfsScheduler::Params{};

    auto result = Explore(params, [] {
      struct Widget {};

      twist::test::WaitGroup{}
          .Add(2, [&] {
            Widget* w = twist::assist::New<Widget>();
            delete w;
          })
          .Join();
    });

    // 1 1 2 2
    // 1 2 1 2
    // 1 2 2 1
    // + symmetric
    assert(result.count == 6);
  }

  {
    auto params = twist::sim::sched::DfsScheduler::Params{};

    auto result = Explore(params, [] {
      struct Widget {
        void Foo() {}
      };

      Widget* w = new Widget{};

      twist::test::WaitGroup{}
          .Add([w] {
            delete w;
          })
          .Add([w] {
            twist::assist::Ptr(w)->Foo();
          })
          .Join();
    });

    assert(result.crash);
  }

  {
    // Spurious compare_exchnage_weak failure

    auto params = twist::sim::sched::DfsScheduler::Params{};
    params.spurious_failures = true;

    auto result = Explore(params, [] {
      twist::ed::std::atomic<int> atom{0};

      int e = 0;
      if (!atom.compare_exchange_weak(e, 1)) {
        twist::ed::system::Abort();
      }
    });

    assert(result.crash);
  }

  return 0;
}
